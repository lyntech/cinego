package com.lyntech.cinegoapi.Utils.Filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.lyntech.cinegoapi.Utils.ConfigReader;
import java.io.IOException;
import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 * Middleware pour les routes authentifié
 * @author manug
 */
@Provider
@Priority(Priorities.AUTHORIZATION)
@AuthBinder
public class AuthFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext context) throws IOException {
        if (context.getHeaderString("x-token") != null) {
            try {
                ConfigReader config = new ConfigReader();
                Algorithm algorithm = Algorithm.HMAC256(config.getConfig("jwt.secret"));
                JWTVerifier verifier = JWT.require(algorithm).build();
                DecodedJWT jwt = verifier.verify(context.getHeaderString("x-token"));
                
                context.setProperty("jwtData", jwt);
            } catch (JWTVerificationException e){
                context.abortWith(Response.status(Response.Status.FORBIDDEN).entity("bye").build());
            }
        }
        else{
            context.abortWith(Response.status(Response.Status.FORBIDDEN).entity("bye").build());
        }
    }
}
