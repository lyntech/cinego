package com.lyntech.cinegoapi.Utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Parser & Reader de la config
 * @author manug
 */
public class ConfigReader {
    String result = "";
    InputStream inputStream;

    public String getConfig(String theprop) throws IOException {
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("Fichier introuvable: '" + propFileName);
            }
            
            result = prop.getProperty(theprop);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            inputStream.close();
        }
        return result;
    }
}
