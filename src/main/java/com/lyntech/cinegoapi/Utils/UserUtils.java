package com.lyntech.cinegoapi.Utils;

import com.lyntech.cinegoapi.DAO.UserDAO;
import org.mindrot.jbcrypt.BCrypt;

/**
 * Class utilitaire pour les utilisateurs
 * @author manug
 */
public class UserUtils {
    private static int workload = 12;
    
    /**
     * Pass hasher
     * @param pass
     * @return Response Object
     * @author Manu
     */
    public static String hashPass(String pass){
        String salt = BCrypt.gensalt(workload);
        String hash = BCrypt.hashpw(pass, salt);

        return hash;
    }
    
    /**
     * Hash - Pass checker
     * @param pass
     * @param hash
     * @return Response Object
     * @author Manu
     */
    public static boolean checkPass(String pass, String hash) {
        boolean retour = false;

        if(null == hash || !hash.startsWith("$2a$")){
            //throw new java.lang.IllegalArgumentException("Invalid hash provided for comparison");
            retour = false;
        }
        retour = BCrypt.checkpw(pass, hash);

        return retour;
    }
    
    /**
     * Set pass
     * @param pass
     * @return Response Object
     * @author Manu
     */
    public static boolean setPass(String pass, int idUser){
        UserDAO UDAO = new UserDAO();
        UDAO.updatePass(UserUtils.hashPass(pass), idUser);
        return true;
    }
}
