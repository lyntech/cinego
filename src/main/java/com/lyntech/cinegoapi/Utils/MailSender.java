package com.lyntech.cinegoapi.Utils;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.*;
import javax.mail.internet.*;

/**
 *  Permet d'envoyer des mails
 * @author manug
 */
public class MailSender {
    ConfigReader config = new ConfigReader();
    
    public Boolean sendMail(String to, int idresa, int type){
        Boolean success = false;
        Transport transport = null;
        try{
            String host = config.getConfig("mail.host");
            String from = config.getConfig("mail.from");
            String pass = config.getConfig("mail.pass");
            
            
            /*Properties properties = System.getProperties();
            properties.setProperty("mail.smtp.host", host);
            properties.setProperty("mail.user", from);
            properties.setProperty("mail.password", pass);
            Session session = Session.getDefaultInstance(properties);
            
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject("Place Cinego - Reservation "+idresa);

            message.setText("Bonjour,\n Voici votre qr code:\n \n \n Au revoir !");

            Transport.send(message);*/
            Properties props = System.getProperties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.port", "587"); 
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.auth", "true");

            Session session = Session.getDefaultInstance(props);
 
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(from,"CINEGO"));
            msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
            
            if(type == 0){//c'est une creation de compte
                msg.setSubject("CINEGO - Création de compte");
                msg.setContent("Bienvenue !,<br> Votre compte cinégo a bien été initialisé.<br> Au revoir !","text/html");
            }
            else{//c'est une reservation
                msg.setSubject("CINEGO - Reservation "+idresa);
                msg.setContent("Bonjour,<br> Voici votre qr code:<br><br><br> Au revoir !","text/html");
            }
            

            //msg.setHeader("X-SES-CONFIGURATION-SET", CONFIGSET);

            transport = session.getTransport();

            System.out.println("Sending...");
            
            transport.connect(host, from, pass);
        	
            transport.sendMessage(msg, msg.getAllRecipients());
            System.out.println("Email sent!");
            
        } catch (MessagingException mex) {
            mex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(MailSender.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            try {
                transport.close();
            } catch (MessagingException ex) {
                System.out.println(ex.getMessage());
            }
        }
        
        return success;
    }
    
}
