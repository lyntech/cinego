package com.lyntech.cinegoapi.Utils.WS;

import com.lyntech.cinegoapi.Model.Films;
import com.lyntech.cinegoapi.Model.FilmsCasts;
import com.lyntech.cinegoapi.Model.FilmsVideos;
import com.lyntech.cinegoapi.Utils.ConfigReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 * HTTP client TMDB
 * @author manug
 */
public class MovieDB {
    ConfigReader config = new ConfigReader();
    public List<Films> searchFilms(String query) throws IOException{
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(config.getConfig("movie.apiurl")+"search/movie?api_key="+config.getConfig("movie.apikey")+"&language=fr&query="+query);
        JsonObject response = target.request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        //System.out.println(response.getJsonArray("result"));
        JsonArray JArray = response.getJsonArray("results");
        //response = response.getJsonArray(3);
        List<Films> filmlist = new ArrayList<>();
        for(JsonValue jsonVal : JArray){
            JsonObject JObject = JArray.getJsonObject(JArray.indexOf(jsonVal));
            System.out.println(JObject);
            filmlist.add(new Films(JObject.getInt("id", 0), JObject.getString("title", ""), JObject.getString("release_date",""), JObject.getString("poster_path","")));            
        }
       
        return filmlist;
    }
    
    public Films getFilmsInfos(int id) throws IOException{
        Client client = ClientBuilder.newClient();
        System.out.println(config.getConfig("movie.apiurl")+"movie/"+id+"?api_key="+config.getConfig("movie.apikey")+"&language=fr");
        WebTarget target = client.target(config.getConfig("movie.apiurl")+"movie/"+id+"?api_key="+config.getConfig("movie.apikey")+"&language=fr");
        JsonObject JObject = target.request(MediaType.APPLICATION_JSON).get(JsonObject.class);
       
        return new Films(JObject.getInt("id", 0), JObject.getString("title", ""), JObject.getString("release_date",""), JObject.getString("poster_path",""), JObject.getString("overview",""));
    }
    
    public List<FilmsVideos> getFilmsVideos(int id) throws IOException{
        Client client = ClientBuilder.newClient();
        System.out.println(config.getConfig("movie.apiurl")+"movie/"+id+"/videos?api_key="+config.getConfig("movie.apikey")+"&language=fr");
        WebTarget target = client.target(config.getConfig("movie.apiurl")+"movie/"+id+"/videos?api_key="+config.getConfig("movie.apikey")+"&language=fr");
        JsonObject response = target.request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        JsonArray JArray = response.getJsonArray("results");
        List<FilmsVideos> filmsVideosList = new ArrayList<>();
        for(JsonValue jsonVal : JArray){
            JsonObject JObject = JArray.getJsonObject(JArray.indexOf(jsonVal));
            filmsVideosList.add(new FilmsVideos(JObject.getString("id", ""), JObject.getString("key", ""), JObject.getString("name", ""), JObject.getString("type", "")));
        }
        return filmsVideosList;
    }
    
    public List<FilmsCasts> getFilmsCasts(int id) throws IOException{;
        Client client = ClientBuilder.newClient();
        System.out.println(config.getConfig("movie.apiurl")+"movie/"+id+"/credits?api_key="+config.getConfig("movie.apikey"));
        WebTarget target = client.target(config.getConfig("movie.apiurl")+"movie/"+id+"/credits?api_key="+config.getConfig("movie.apikey"));
        JsonObject response = target.request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        JsonArray JArray = response.getJsonArray("cast");
        List<FilmsCasts> filmsCastsList = new ArrayList<>();
        for(JsonValue jsonVal : JArray){
            JsonObject JObject = JArray.getJsonObject(JArray.indexOf(jsonVal));
            filmsCastsList.add(new FilmsCasts(JObject.getInt("cast_id", 0), JObject.getString("character", ""), JObject.getInt("id", 0), JObject.getString("name", ""), JObject.getString("profile_path", "")));
        }
        System.out.println(filmsCastsList);
        return filmsCastsList;
    }
}
