/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Routes;

import com.lyntech.cinegoapi.DAO.SeanceDAO;
import com.lyntech.cinegoapi.Model.Seance;
import com.lyntech.cinegoapi.Model.Tarif;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import static javax.json.Json.createObjectBuilder;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Yoann
 */
@Path("/")
public class SeanceRoutes {
    @Context
    private UriInfo context;
    
    private SeanceDAO SeDAO;

    public SeanceRoutes() {
        this.SeDAO = new SeanceDAO();
    }

    /**
     * @param idSeance Id de la séance
     * @return Retourne un objet Séance
     */
    @GET
    @Path("seance/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSeance(@PathParam("id") int idSeance) 
    {
        JsonObject rep = null;
        try
        {
            rep = createObjectBuilder().add("success", true).add("content",SeDAO.get(idSeance).toJsonObject()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }

    /**
     * @return Retourne une list d'objet Séance
     */
    @GET
    @Path("seances")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSeances() 
    {
        JsonObject rep = null;
        try
        {
            JsonArrayBuilder JsonABuilder = Json.createArrayBuilder();
            SeDAO.getAll().forEach((uneSeance)->{
                try {
                    JsonABuilder.add(uneSeance.toJsonObject());
                } catch (IOException ex) {
                    Logger.getLogger(SeanceRoutes.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            rep = createObjectBuilder().add("success", true).add("content",JsonABuilder.build()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }

    /**
     * @return le résultat de l'ajout : Correct || Non correcte
     */
    @POST
    @Path("seance")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postSeance(@FormParam("date") String date,@FormParam("movie") int idFilm,@FormParam("room") int idSalle)
    {
        JsonObject rep = null;
        try
        {
            rep = createObjectBuilder().add("success", true).add("content",SeDAO.create(new Seance(date,idFilm,idSalle))).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    /**
     * @param idSeance Id de la séance
     * @param date Date de la séance
     * @param idFilm Id du film
     * @param idSalle Id de la salle
     * @return le résultat de la modif : Correct || Non correcte
     */
    @PUT
    @Path("seance/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response putSeance(@PathParam("id") int idSeance,@FormParam("date") String date,@FormParam("movie") int idFilm,@FormParam("room") int idSalle)
    {   
        JsonObject rep = null;
        try
        {
            Seance modifSeance = new Seance(idSeance,date,idFilm,idSalle);
            SeDAO.update(modifSeance);
            rep = createObjectBuilder().add("success", true).add("content",modifSeance.getIdSeance()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();

    }
    /**
     * @param Seances Chaine de caractère de Séance
     * @return le résultat de la modif : Correct || Non correcte
     */
    @PUT
    @Path("seances")
    @Produces(MediaType.APPLICATION_JSON)
    public Response putSeances(@FormParam("seances") String Seances)
    {
        JsonObject rep = null;
        try
        {
            JsonArrayBuilder JArray = Json.createArrayBuilder();//JSON qui contient les ID de chaque update
            
            JsonArray JArraySeances = Json.createReader(new StringReader(Seances)).readArray();//Tableau des objets JSON
            List<Seance> modifSeances = new ArrayList<>();//Création de la liste d'objet
            for (JsonValue jsonVal : JArraySeances)//Pour chaque objet JSON
            {
               JsonObject JObject = JArraySeances.getJsonObject(JArraySeances.indexOf(jsonVal));//On récupére l'objet JSON du tableau
               modifSeances.add(new Seance(JObject.getInt("id"),JObject.getString("date"),JObject.getInt("idFilm"),JObject.getInt("idSalle")));//On transforme l'objet JSON en objet JAVA
               JArray.add(modifSeances.get(modifSeances.size()-1).getIdSeance());//On ajoute l'ID de l'objet aux tableau JSON des updates
            }
            SeDAO.updateMultiple(modifSeances);
            rep = createObjectBuilder().add("success", true).add("content",JArray.build()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    /**
     * @param idSeance Id de la séance
     * @return le résultat de la modif : Correct || Non correcte
     */
    @DELETE
    @Path("seance/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeSeance(@PathParam("id") int idSeance) 
    {        
        JsonObject rep = null;
        try
        {
            SeDAO.delete(SeDAO.get(idSeance));
            rep = createObjectBuilder().add("success", true).add("content",idSeance).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }    
}
