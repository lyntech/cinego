/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Routes;

import com.lyntech.cinegoapi.DAO.SalleDAO;
import com.lyntech.cinegoapi.Model.Salle;
import java.io.IOException;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.json.Json;
import static javax.json.Json.createObjectBuilder;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;

/**
 * @author TERUEL
 */
@Path("/")
public class SalleRoute {

    @Context
    private UriInfo context;

    private SalleDAO SDAO;

    public SalleRoute() {
        this.SDAO = new SalleDAO();
    }

    /**
     * Récupère une salle en fonction de l'id
     * @param idSalle id de la salle
     * @return un objet Salle
     */
    @GET
    @Path("salle/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJson(@PathParam("id") int idSalle) {
        JsonObject rep;
        try
        {
            rep = createObjectBuilder().add("success", true).add("content",SDAO.get(idSalle).toJsonObject()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();

    }
    /**
     * Récupère une liste de salles
     * @return une liste d'objet Salles
     */
    @GET
    @Path("salles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRooms() {
        JsonObject rep;
        try
        {
            JsonArrayBuilder JsonABuilder = Json.createArrayBuilder();
            SDAO.getAll().forEach((uneSalle)->{
                try{
                    JsonABuilder.add(uneSalle.toJsonObject());
                }catch(IOException ex){
                    Logger.getLogger(SalleRoute.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            rep = createObjectBuilder().add("success", true).add("content",JsonABuilder.build()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();

    }
    /**
     * Ajout une salle depuis un formulaire
     * @param nomSalle nom de la salle
     * @param planSalle plan de la salle en format XML
     * @param nbPlaceSalle nombre de place en Int
     * @param idCinema id de l'objet Cinema
     * @param idTechno id de l'objet Techno
     * @return le résultat de la modif : Correct || Non correcte
     */
    @POST
    @Path("salle")
    public Response postSalle(@FormParam("name") String nomSalle, @FormParam("blueprint") String planSalle, @FormParam("placelenght") int nbPlaceSalle, @FormParam("cinema") int idCinema, @FormParam("techno") int idTechno) {
        JsonObject rep;
        try
        {
            System.out.println(nomSalle+planSalle+nbPlaceSalle+idCinema+idTechno);
            rep = createObjectBuilder().add("success", true).add("content", SDAO.create(new Salle(nomSalle, planSalle, nbPlaceSalle, idCinema, idTechno))).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    /**
     * Modifie une salle
     * @param idSalle id de la salle
     * @param nomSalle nom de la salle
     * @param planSalle plan de la salle en format XML
     * @param nbPlaceSalle nombre de place en Int
     * @param idCinema id de l'objet Cinema
     * @param idTechno id de l'objet Techno
     * @return le résultat de la modif : Correct || Non correcte
     */
    @PUT
    @Path("salle/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response putSalle(@PathParam("id") int idSalle, @FormParam("name") String nomSalle, @FormParam("blueprint") String planSalle, @FormParam("placelenght") int nbPlaceSalle, @FormParam("cinema") int idCinema, @FormParam("techno") int idTechno) {
        JsonObject rep;
        try
        {
            System.out.println(idSalle+nomSalle+planSalle+nbPlaceSalle+idCinema+idTechno);
            Salle modifSalle = new Salle(idSalle,nomSalle,planSalle, nbPlaceSalle,idCinema, idTechno);
            SDAO.update(modifSalle);
            rep = createObjectBuilder().add("success", true).add("content",modifSalle.getIdSalle()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    /**
     * Modifie un ensemble de salles
     * @param Salles Chaine de caractère de salles
     * @return le résultat de la modif : Correct || Non correcte
     */
    @PUT
    @Path("salles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response putSalles(@FormParam("salles") String Salles)
    {
        JsonObject rep;
        try
        {
            JsonArrayBuilder JArray = Json.createArrayBuilder(); //JSON qui contient les ID de chaque update

            JsonArray JArraySalles = Json.createReader(new StringReader(Salles)).readArray(); //Tableau des objets JSON
            List<Salle> modifSalles = new ArrayList<>(); //Création de la liste d'objet
            for (JsonValue jsonVal : JArraySalles) //Pour chaque objet JSON
            {
                JsonObject JObject = JArraySalles.getJsonObject(JArraySalles.indexOf(jsonVal)); //On récupére l'objet JSON du tableau
                modifSalles.add(new Salle(
                        JObject.getInt("idSalle"),
                        JObject.getString("nom"),
                        JObject.getString("plan"),
                        JObject.getInt("nbPlace"),
                        JObject.getInt("idCinema"),
                        JObject.getInt("idTechno")
                )); //On transforme l'objet JSON en objet JAVA
                JArray.add(modifSalles.get(modifSalles.size()-1).getIdSalle()); //On ajoute l'id de l'objet aux tableau JSON des updates
            }
            SDAO.updateMultiple(modifSalles);
            rep = createObjectBuilder().add("success", true).add("content",JArray.build()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    /**
     * Supprime une salle
     * @param idSalle id de la salle
     * @return le résultat de la modif : Correct || Non correcte
     */
    @DELETE
    @Path("salle/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSalle(@PathParam("id") int idSalle) {
        JsonObject rep;
        try
        {
            SDAO.deletebyid(idSalle);
            rep = createObjectBuilder().add("success", true).add("content",idSalle).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }

    @PUT
    @Path("salle/{id}/blueprint")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePlan(@PathParam("id") int id, @FormParam("blueprint") String plan) {
        JsonObject rep;
        try
        {
            SDAO.updatePlan(plan, id);
            rep = createObjectBuilder().add("success", true).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
}
