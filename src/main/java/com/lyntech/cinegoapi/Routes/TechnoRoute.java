/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Routes;

import com.lyntech.cinegoapi.DAO.TechnoDAO;
import com.lyntech.cinegoapi.Model.Salle;
import com.lyntech.cinegoapi.Model.Techno;

import javax.json.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import static javax.json.Json.createObjectBuilder;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author TERUEL
 */
@Path("/")
public class TechnoRoute {

    @Context
    private UriInfo context;

    private TechnoDAO TechDAO;

    public TechnoRoute() {
        this.TechDAO = new TechnoDAO();
    }

    /**
     * @param idTechno id de la techno
     * @return un objet Techno
     */
    @GET
    @Path("techno/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTechno(@PathParam("id") int idTechno) {
        JsonObject rep;
        try
        {
            rep = createObjectBuilder().add("success", true).add("content",TechDAO.get(idTechno).toJsonObject()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }

    /**
     * @return une liste d'objet Techno
     */
    @GET
    @Path("technos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTechnos() {
        JsonObject rep;
        try
        {
            JsonArrayBuilder JsonABuilder = Json.createArrayBuilder();
            TechDAO.getAll().forEach((uneTechno)->{
                JsonABuilder.add(uneTechno.toJsonObject());
            });
            rep = createObjectBuilder().add("success", true).add("content",JsonABuilder.build()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    /**
     * Ajout une techno depuis un formulaire
     * @param nomTechno nom de la techno
     * @return le résultat de la modif : Correct || Non correcte
     */
    @POST
    @Path("techno")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postTechno(@FormParam("nomTechno") String nomTechno) {
        JsonObject rep;
        try
        {
            rep = createObjectBuilder().add("success", true).add("content", TechDAO.create(new Techno(nomTechno))).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    /**
     * Modifie une techno
     * @param idTechno id de la techno
     * @param nomTechno nom de la techno
     * @return le résultat de la modif : Correct || Non correcte
     */
    @PUT
    @Path("techno/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response putTechno(@PathParam("id") int idTechno, @FormParam("nomTechno") String nomTechno) {
        JsonObject rep;
        try
        {
            Techno modifTechno = new Techno(idTechno,nomTechno);
            TechDAO.update(modifTechno);
            rep = createObjectBuilder().add("success", true).add("content", modifTechno.getIdTechno()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    /**
     * Modifie un ensemble de technos
     * @param Technos Chaine de caractère de technos
     * @return le résultat de la modif : Correct || Non correcte
     */
    @PUT
    @Path("technos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response putTechnos(@FormParam("technos") String Technos)
    {
        JsonObject rep;
        try
        {
            JsonArrayBuilder JArray = Json.createArrayBuilder(); //JSON qui contient les ID de chaque update

            JsonArray JArrayTechnos = Json.createReader(new StringReader(Technos)).readArray(); //Tableau des objets JSON
            List<Techno> modifTechnos = new ArrayList<>(); //Création de la liste d'objet
            for (JsonValue jsonVal : JArrayTechnos) //Pour chaque objet JSON
            {
                JsonObject JObject = JArrayTechnos.getJsonObject(JArrayTechnos.indexOf(jsonVal)); //On récupére l'objet JSON du tableau
                modifTechnos.add(new Techno(
                        JObject.getInt("idTechno"),
                        JObject.getString("nom")
                )); //On transforme l'objet JSON en objet JAVA
                JArray.add(modifTechnos.get(modifTechnos.size()-1).getIdTechno()); //On ajoute l'id de l'objet aux tableau JSON des updates
            }
            TechDAO.updateMultiple(modifTechnos);
            rep = createObjectBuilder().add("success", true).add("content",JArray.build()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }

    /**
     * Supprime une techno
     * @param idTechno id de la techno
     * @return le résultat de la modif : Correct || Non correcte
     */
    @DELETE
    @Path("techno/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteTechno(@PathParam("id") int idTechno) {
        JsonObject rep = createObjectBuilder().add("success", true).add("test","tessss").build();
        try
        {
            TechDAO.deletebyid(idTechno);
            rep = createObjectBuilder().add("success", true).add("content",idTechno).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
}
