package com.lyntech.cinegoapi.Routes;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.lyntech.cinegoapi.DAO.UserDAO;
import com.lyntech.cinegoapi.Model.User;
import com.lyntech.cinegoapi.Utils.ConfigReader;
import com.lyntech.cinegoapi.Utils.UserUtils;
import com.sun.tools.javac.util.Pair;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import static javax.json.Json.createObjectBuilder;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

/**
 * Routes d'auth
 * @author manug
 */
@Path("/")
public class AuthRoutes {
    
    @Context
    private UriInfo context;
    private UserDAO UDAO = new UserDAO();
    
    /**
     *  Route de connexion
     * @param login
     * @param pass
     * @return Response Object
     * @throws IOException
     * @author Manu
     */
    @POST
    @Path("login")
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(@FormParam("login") String login, @FormParam("password") String pass) throws IOException{
        Response retour = null;
        Pair result = (Pair) UDAO.getUserbyLogin(login);
        Boolean success = (Boolean)result.fst;
        User theUser = (User)result.snd;
        System.out.println(pass);
        System.out.println(theUser.getPass());
        if(success){
            if(UserUtils.checkPass(pass, theUser.getPass())){
                ConfigReader config = new ConfigReader();
                Algorithm algorithm = Algorithm.HMAC256(config.getConfig("jwt.secret"));
                
                Date time = new java.util.Date();
                Calendar c = Calendar.getInstance();
                c.setTime(time);
                c.add(Calendar.HOUR, 1);
                Date timeEnd = c.getTime();
                
                String token = JWT.create().withClaim("id", theUser.getIdUser()).withClaim("login", theUser.getLogin()).withClaim("nom", theUser.getNom()).withClaim("prenom", theUser.getPrenom()).withClaim("rank", theUser.getRank()).withIssuedAt(time).withExpiresAt(timeEnd).sign(algorithm);
                retour = Response.ok(createObjectBuilder().add("success", true).add("token", token).build()).build();
            }
            else{
                retour = Response.status(404).entity(createObjectBuilder().add("success", false).add("error", "bad pass").build()).build();
            }
        }
        else{
            retour = Response.status(404).entity(createObjectBuilder().add("success", false).add("error", "bad user").build()).build();
        }
        
        return retour;
    }
}
