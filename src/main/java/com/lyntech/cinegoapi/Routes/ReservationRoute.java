/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Routes;

import com.lyntech.cinegoapi.DAO.ReservationDAO;
import com.lyntech.cinegoapi.DAO.SeanceDAO;
import com.lyntech.cinegoapi.Model.PlaceReserver;
import com.lyntech.cinegoapi.Model.Reservation;
import com.lyntech.cinegoapi.Model.Seance;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import static javax.json.Json.createObjectBuilder;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Yoann
 */
@Path("/")
public class ReservationRoute {
    @Context
    private UriInfo context;
    private ReservationDAO RDAO;

    public ReservationRoute() {
        this.RDAO = new ReservationDAO();
    }
    
    @POST
    @Path("reservation")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postReservation(@FormParam("date") String date,@FormParam("sceance") int idSeance, @FormParam("user") int idUtilisateur)
    {
        System.out.print(idUtilisateur + date + idSeance);
        JsonObject rep = null;
        try
        {
            rep = createObjectBuilder().add("success", true).add("content",RDAO.create(new Reservation(idUtilisateur,date,idSeance))).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    
    @POST
    @Path("reservation/place")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postPlaceReservation(@FormParam("idPlace") int idPlace,@FormParam("reservation") int idReservation, @FormParam("prix") float prix)
    {
        JsonObject rep = null;
        try
        {
            rep = createObjectBuilder().add("success", true).add("content",RDAO.createPlace(new PlaceReserver(idPlace, idReservation, prix))).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    
    @GET
    @Path("reservation/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReservation(@PathParam("id") int idReservation) 
    {
        JsonObject rep = null;
        try
        {
            rep = createObjectBuilder().add("success", true).add("content", RDAO.get(idReservation).toJsonObject()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    
    @GET
    @Path("reservations/user/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReservationByUser(@PathParam("id") int idUser) 
    {
        JsonObject rep = null;
        try
        {
            JsonArrayBuilder JsonABuilder = Json.createArrayBuilder();
            RDAO.getReservationsByUser(idUser).forEach((uneReservation)->{
                try {
                    JsonABuilder.add(uneReservation.toJsonObject());
                } catch (IOException ex) {
                    Logger.getLogger(ReservationRoute.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            rep = createObjectBuilder().add("success", true).add("content",JsonABuilder.build()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }

    @GET
    @Path("reservations")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSeances() 
    {
        JsonObject rep = null;
        try
        {
            JsonArrayBuilder JsonABuilder = Json.createArrayBuilder();
            RDAO.getAll().forEach((uneReservation)->{
                try {
                    JsonABuilder.add(uneReservation.toJsonObject());
                } catch (IOException ex) {
                    Logger.getLogger(ReservationRoute.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            rep = createObjectBuilder().add("success", true).add("content",JsonABuilder.build()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }

    @PUT
    @Path("reservation/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response putReservation(@PathParam("id") int idReservation,@FormParam("idUtilisateur") int idUtilisateur,@FormParam("date") String date,@FormParam("idSeance") int idSeance)
    {   
        JsonObject rep = null;
        try
        {
            Reservation modifReservation = new Reservation(idReservation,idUtilisateur,date,idSeance);
            RDAO.update(modifReservation);
            rep = createObjectBuilder().add("success", true).add("content",modifReservation.getIdReservation()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();

    }
    
    @PUT
    @Path("reservations")
    @Produces(MediaType.APPLICATION_JSON)
    public Response putReservations(@FormParam("reservations") String Reservations)
    {
        JsonObject rep = null;
        try
        {
            JsonArrayBuilder JArray = Json.createArrayBuilder();//JSON qui contient les ID de chaque update
            
            JsonArray JArrayReservations = Json.createReader(new StringReader(Reservations)).readArray();//Tableau des objets JSON
            List<Reservation> modifReservations = new ArrayList<>();//Création de la liste d'objet
            for (JsonValue jsonVal : JArrayReservations)//Pour chaque objet JSON
            {
               JsonObject JObject = JArrayReservations.getJsonObject(JArrayReservations.indexOf(jsonVal));//On récupére l'objet JSON du tableau
               modifReservations.add(new Reservation(JObject.getInt("id"),JObject.getInt("idUtilisateur"),JObject.getString("date"),JObject.getInt("idSeance")));//On transforme l'objet JSON en objet JAVA
               JArray.add(modifReservations.get(modifReservations.size()-1).getIdReservation());//On ajoute l'ID de l'objet aux tableau JSON des updates
            }
            RDAO.updateMultiple(modifReservations);
            rep = createObjectBuilder().add("success", true).add("content",JArray.build()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    
    @DELETE
    @Path("reservation/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeReservation(@PathParam("id") int idReservation) 
    {        
        JsonObject rep = null;
        try
        {
            RDAO.delete(RDAO.get(idReservation));
            rep = createObjectBuilder().add("success", true).add("content",idReservation).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }       
}
