/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Routes;

import com.lyntech.cinegoapi.DAO.CinemaDAO;
import com.lyntech.cinegoapi.DAO.EnseigneDAO;
import com.lyntech.cinegoapi.DAO.TarifDAO;
import com.lyntech.cinegoapi.Model.Enseigne;
import com.lyntech.cinegoapi.Utils.Filter.AuthBinder;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import static javax.json.Json.createObjectBuilder;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
/**
 * REST Web Service
 *
 * @author Switch
 */
@Path("/")
public class EnseigneRoutes {

    @Context
    private UriInfo context;
    private CinemaDAO CDAO;
    private TarifDAO TDAO;
    private EnseigneDAO EDAO;

    /**
     * Creates a new instance of Main
     */
    public EnseigneRoutes() {
        this.CDAO = new CinemaDAO();
        this.TDAO = new TarifDAO();
        this.EDAO = new EnseigneDAO();
    }

    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("enseigne")
    @AuthBinder
    public Response postEnseigne(@FormParam("name") String nom)
    {        
        JsonObject rep = null;
        try
        {
            rep = createObjectBuilder().add("success", true).add("content",EDAO.create(new Enseigne(nom))).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    
    /**
     *
     * @param idEnseigne
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("enseigne/{id}")
    @AuthBinder
    public Response getEnseigne(@PathParam("id") int idEnseigne) {
        JsonObject rep = null;
        try
        {
            rep = createObjectBuilder().add("success", true).add("content",EDAO.get(idEnseigne).toJsonObject()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }

    /**
     *
     * @return
     */
    @GET
    @Path("enseignes")
    @Produces(MediaType.APPLICATION_JSON)
    @AuthBinder
    public Response getEnseignes() {
        
        JsonObject rep = null;
        try
        {
            JsonArrayBuilder JsonABuilder = Json.createArrayBuilder();
            EDAO.getAll().forEach((uneEnseigne)->{
                try{
                    JsonABuilder.add(uneEnseigne.toJsonObject());
                } catch(IOException ex){;
                    Logger.getLogger(EnseigneRoutes.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            rep = createObjectBuilder().add("success", true).add("content",JsonABuilder.build()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
       
    /**
     *
     * @param idEnseigne
     * @param nom
     * @return
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("enseigne/{id}")
    @AuthBinder
    public Response putEnseigne(@PathParam("id") int idEnseigne,@FormParam("name") String nom) 
    {
        JsonObject rep = null;
        try
        {
            Enseigne modifEnseigne = new Enseigne(idEnseigne,nom);
            EDAO.update(modifEnseigne);
            rep = createObjectBuilder().add("success", true).add("content",modifEnseigne.getIdEnseigne()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    
    /**
     *
     * @param Enseignes
     * @return
     */
    @PUT
    @Path("enseignes")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @AuthBinder
    public Response putEnseignes(@FormParam("enseignes") String Enseignes) {
        JsonObject rep = null;
        try
        {
            JsonArrayBuilder JArray = Json.createArrayBuilder();//JSON qui contient les ID de chaque update
            
            JsonArray JArrayEnseigne = Json.createReader(new StringReader(Enseignes)).readArray();//Tableau des objets JSON
            List<Enseigne> modifEnseigne = new ArrayList<>();//Création de la liste d'objet
            for (JsonValue jsonVal : JArrayEnseigne)//Pour chaque objet JSON
            {
               JsonObject JObject = JArrayEnseigne.getJsonObject(JArrayEnseigne.indexOf(jsonVal));//On récupére l'objet JSON du tableau
               modifEnseigne.add(new Enseigne(JObject.getInt("ID"),JObject.getString("nom")));//On transforme l'objet JSON en objet JAVA
               JArray.add(modifEnseigne.get(modifEnseigne.size()-1).getIdEnseigne());//On ajoute l'ID de l'objet aux tableau JSON des updates
            }
            EDAO.updateMultiple(modifEnseigne);
            rep = createObjectBuilder().add("success", true).add("content",JArray.build()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    
    /**
     *
     * @param idEnseigne
     * @return
     */
    @DELETE
    @Path("enseigne/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @AuthBinder
    public Response supprEnseigne(@PathParam("id") int idEnseigne) 
    {
        JsonObject rep = null;
        try
        {
            EDAO.deletebyid(idEnseigne);
            rep = createObjectBuilder().add("success", true).add("content",idEnseigne).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
}
