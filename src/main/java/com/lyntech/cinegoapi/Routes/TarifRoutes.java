/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Routes;

import com.lyntech.cinegoapi.DAO.CinemaDAO;
import com.lyntech.cinegoapi.DAO.TarifDAO;
import com.lyntech.cinegoapi.Model.Tarif;
import com.lyntech.cinegoapi.Utils.Filter.AuthBinder;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import static javax.json.Json.createObjectBuilder;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
/**
 * REST Web Service
 *
 * @author Switch
 */
@Path("/")
public class TarifRoutes {

    @Context
    private UriInfo context;
    
    private TarifDAO TDAO;
    private CinemaDAO CDAO;

    /**
     * Creates a new instance of Main
     */
    public TarifRoutes() {
        this.CDAO = new CinemaDAO();
        this.TDAO = new TarifDAO();
    }

    /**
     * Retrieves representation of an instance of com.lyntech.cinegoapi.Main
     * @return an instance of java.lang.String
     */
    @POST
    @Path("tarif")
    @Produces(MediaType.APPLICATION_JSON)
    @AuthBinder
    public Response postTarif(@FormParam("nom") String nom,@FormParam("prix") float prix,@FormParam("cinema") int idCinema)
    {
        JsonObject rep = null;
        try
        {
            rep = createObjectBuilder().add("success", true).add("content",TDAO.create(new Tarif(nom,prix,idCinema))).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    
    /**
     *
     * @param idTarif
     * @return
     */
    @GET
    @Path("tarif/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @AuthBinder
    public Response getTarif(@PathParam("id") int idTarif) 
    {
        JsonObject rep = null;
        try
        {
            rep = createObjectBuilder().add("success", true).add("content",TDAO.get(idTarif).toJsonObject()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }

    /**
     *
     * @return
     */
    @GET
    @Path("tarifs")
    @Produces(MediaType.APPLICATION_JSON)
    @AuthBinder
    public Response getTarifs() 
    {
        JsonObject rep = null;
        try
        {
            JsonArrayBuilder JsonABuilder = Json.createArrayBuilder();
            TDAO.getAll().forEach((unTarif)->{
                try{
                    JsonABuilder.add(unTarif.toJsonObject());
                } catch (IOException ex){
                    Logger.getLogger(TarifRoutes.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            });
            rep = createObjectBuilder().add("success", true).add("content",JsonABuilder.build()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    
    /**
     *
     * @return
     */
    @GET
    @Path("tarifs/cinema/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTarifsByIdCinema(@PathParam("id") int idCinema) 
    {
        JsonObject rep = null;
        try
        {
            JsonArrayBuilder JsonABuilder = Json.createArrayBuilder();
            TDAO.getCinemaTarifs(idCinema).forEach((unTarif)->{
                try{
                    JsonABuilder.add(unTarif.toJsonObject());
                } catch (IOException ex){
                    Logger.getLogger(TarifRoutes.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            });
            rep = createObjectBuilder().add("success", true).add("content",JsonABuilder.build()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }

    /**
     *
     * @param idTarif
     * @param nom
     * @param prix
     * @param idCinema
     * @return
     */
    @PUT
    @Path("tarif/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @AuthBinder
    public Response putTarif(@PathParam("id") int idTarif,@FormParam("nom") String nom,@FormParam("prix") float prix,@FormParam("cinema") int idCinema)
    {   
        JsonObject rep = null;
        try
        {
            Tarif modifTarif = new Tarif(idTarif,nom,prix,idCinema);
            TDAO.update(modifTarif);
            rep = createObjectBuilder().add("success", true).add("content",modifTarif.getIDTarif()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();

    }
    
    /**
     *
     * @param Tarifs
     * @return
     */
    @PUT
    @Path("tarifs")
    @Produces(MediaType.APPLICATION_JSON)
    @AuthBinder
    public Response putTarifs(@FormParam("tarifs") String Tarifs)
    {
        JsonObject rep = null;
        try
        {
            JsonArrayBuilder JArray = Json.createArrayBuilder();//JSON qui contient les ID de chaque update
            
            JsonArray JArrayTarifs = Json.createReader(new StringReader(Tarifs)).readArray();//Tableau des objets JSON
            List<Tarif> modifTarifs = new ArrayList<>();//Création de la liste d'objet
            for (JsonValue jsonVal : JArrayTarifs)//Pour chaque objet JSON
            {
               JsonObject JObject = JArrayTarifs.getJsonObject(JArrayTarifs.indexOf(jsonVal));//On récupére l'objet JSON du tableau
               modifTarifs.add(new Tarif(JObject.getInt("ID"),JObject.getString("nom"),Double.valueOf(JObject.getString("prix")),JObject.getInt("cinema")));//On transforme l'objet JSON en objet JAVA
               JArray.add(modifTarifs.get(modifTarifs.size()-1).getIDTarif());//On ajoute l'ID de l'objet aux tableau JSON des updates
            }
            TDAO.updateMultiple(modifTarifs);
            rep = createObjectBuilder().add("success", true).add("content",JArray.build()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    
    /**
     *
     * @param idTarif
     * @return
     */
    @DELETE
    @Path("tarif/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @AuthBinder
    public Response removeTarif(@PathParam("id") int idTarif) 
    {        
        JsonObject rep = null;
        try
        {
            TDAO.delete(TDAO.get(idTarif));
            rep = createObjectBuilder().add("success", true).add("content",idTarif).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }    
}
