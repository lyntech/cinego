/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Routes;

import com.lyntech.cinegoapi.DAO.CinemaDAO;
import com.lyntech.cinegoapi.Model.Cinema;
import com.lyntech.cinegoapi.Utils.Filter.AuthBinder;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import static javax.json.Json.createObjectBuilder;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
/**
 * REST Web Service
 *
 * @author Switch
 */
@Path("/")
public class CinemaRoutes {

    @Context
    private UriInfo context;
    private CinemaDAO CDAO = new CinemaDAO();

    /**
     * Creates a new instance of Main
     */
    public CinemaRoutes() {
    }

    /**
     * Retrieves representation of an instance of com.lyntech.cinegoapi.Main
     * @return an instance of java.lang.String
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("cinema")
    @AuthBinder
    public Response postCinema(@FormParam("name") String nom,@FormParam("adress") String adresse,@FormParam("signboard") int IDEnseigne)
    {       
        JsonObject rep = null;
        try
        {
            rep = createObjectBuilder().add("success", true).add("content",CDAO.create(new Cinema(nom,adresse,IDEnseigne))).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    
    /**
     *
     * @param idCinema
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("cinema/{id}")
    @AuthBinder
    public Response getCinema(@PathParam("id") int idCinema) {
        JsonObject rep = null;
        try
        {
            rep = createObjectBuilder().add("success", true).add("content",CDAO.get(idCinema).toJsonObject()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }

    /**
     *
     * @return
     */
    @GET
    @Path("cinemas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCinemas() {
        JsonObject rep = null;
        try
        {
            JsonArrayBuilder JsonABuilder = Json.createArrayBuilder();
            CDAO.getAll().forEach((unCinema)->{
                try{
                    JsonABuilder.add(unCinema.toJsonObject());
                } catch(IOException ex){
                    Logger.getLogger(CinemaRoutes.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            rep = createObjectBuilder().add("success", true).add("content",JsonABuilder.build()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }

    /**
     *
     * @param idCinema
     * @param nom
     * @param adresse
     * @param idEnseigne
     * @return
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("cinema/{id}")
    @AuthBinder
    public Response putCinema(@PathParam("id") int idCinema,@FormParam("name") String nom,@FormParam("adress") String adresse,@FormParam("signboard") int idEnseigne)
    {
        JsonObject rep = null;
        System.out.println("ENTER");
        try
        {
            Cinema modifCinema = new Cinema(idCinema,nom,adresse,idEnseigne);
            System.out.println("NEW CINE");
            CDAO.update(modifCinema);
            System.out.println("MODIF");
            rep = createObjectBuilder().add("success", true).add("content",modifCinema.getIdCinema()).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
    
    /**
     *
     * @param idCinema
     * @return
     */
    @DELETE
    @Path("cinema/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @AuthBinder
    public Response supprCinema(@PathParam("id") int idCinema) 
    {
        JsonObject rep = null;
        try
        {
            CDAO.deletebyid(idCinema);
            rep = createObjectBuilder().add("success", true).add("content",idCinema).build();
        }
        catch(Exception e)
        {
            rep = createObjectBuilder().add("success", false).add("error", e.getMessage()).build();
        }
        return Response.ok(rep).build();
    }
}
