package com.lyntech.cinegoapi.Routes;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.lyntech.cinegoapi.DAO.UserDAO;
import com.lyntech.cinegoapi.Model.User;
import com.lyntech.cinegoapi.Utils.Filter.AuthBinder;
import com.lyntech.cinegoapi.Utils.MailSender;
import com.lyntech.cinegoapi.Utils.UserUtils;
import com.sun.tools.javac.util.Pair;
import java.util.ArrayList;
import java.util.List;
import javax.json.Json;
import static javax.json.Json.createObjectBuilder;
import javax.json.JsonArrayBuilder;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *  Routes de gestion des utilisateurs
 * @author manug
 */
@Path("/")
public class UserRoutes {
    @Context
    private UriInfo context;
    private UserDAO UDAO = new UserDAO();
    
    @Context
    private transient HttpServletRequest servletRequest;
    
    /**
     * Récupération des infos utilisateur
     * @param id
     * @return Response Object
     * @author Manu
     */
    @GET
    @Path("user/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @AuthBinder
    public Response getUserInfos(@PathParam("id") int id){
        Response retour = null;
        try
        {
            DecodedJWT jwt = (DecodedJWT) this.servletRequest.getAttribute("jwtData");
            int idUser = jwt.getClaim("id").asInt();
            int rank = jwt.getClaim("rank").asInt();
            
            if(id == idUser || rank >= 50){
                retour = Response.ok(createObjectBuilder().add("success", true).add("user", UDAO.get(id).toJsonObject()).build()).build();
            }
            else{
                throw new Exception("Access Denied");//TODO change it
            }
            
        }
        catch(Exception e)
        {
            retour = Response.status(500).entity(createObjectBuilder().add("success", false).add("error", e.getMessage()).build()).build();
        }
        return retour;
    }
    
    /**
     * Récupération de tout les utilisateurs
     * @return Response Object
     * @author Manu
     */
    @GET
    @Path("users")
    @Produces(MediaType.APPLICATION_JSON)
    @AuthBinder
    public Response getUsers(){
        Response retour = null;
        try
        {
            DecodedJWT jwt = (DecodedJWT) this.servletRequest.getAttribute("jwtData");
            int rank = jwt.getClaim("rank").asInt();
            
            if(rank >= 50){
                JsonArrayBuilder JsonABuilder = Json.createArrayBuilder();
                List<User> userList = new ArrayList<>();
                userList = UDAO.getAll();
                for(User theUser : userList){
                    JsonABuilder.add(theUser.toJsonObject());
                }
                retour = Response.ok(createObjectBuilder().add("success", true).add("users", JsonABuilder.build()).build()).build();
            }
            else{
                throw new Exception("Access Denied");//TODO change it
            }
        }
        catch(Exception e)
        {
            System.out.println(e.toString());
            retour = Response.status(500).entity(createObjectBuilder().add("success", false).add("error", "").build()).build();
        }
        return retour;
    }
    
    /**
     * Création d'un utilisateur
     * @param id
     * @param login
     * @param nom
     * @param prenom
     * @param pass
     * @return Response Object
     * @author Manu
     */
    @POST
    @Path("user")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUser(@FormParam("login") String login, @FormParam("nom") String nom, @FormParam("prenom") String prenom, @FormParam("pass") String pass){
        Response retour = null;
        try
        {
            Pair result = (Pair) UDAO.getUserbyLogin(login);
            Boolean success = (Boolean)result.fst;
            //User theUserfromdb = (User)result.snd;
            
            if(!success){
                String hash = UserUtils.hashPass(pass);
                User theUser = new User(0 , login, nom, prenom, hash);
                int userInserted = UDAO.create(theUser);
                System.out.println(userInserted);
                if(userInserted > 0){
                    MailSender sender = new MailSender();
                    sender.sendMail(login, 0, 0);

                    retour = Response.ok(createObjectBuilder().add("success", true).build()).build();
                }
                else{
                    retour = Response.status(200).entity(createObjectBuilder().add("success", false).add("error", "xxx").build()).build();
                }
                
            }
            else{
                retour = Response.status(200).entity(createObjectBuilder().add("success", false).add("error", "xxx").build()).build();
            }
            
        }
        catch(Exception e)
        {
            System.out.println(e);
            retour = Response.status(500).entity(createObjectBuilder().add("success", false).add("error", "").build()).build();
        }
        return retour;
    }
    
    /**
     * Mise a jour d'un utilisateur
     * @param id
     * @param login
     * @param nom
     * @param prenom
     * @return Response Object
     * @author Manu
     */
    @PUT
    @Path("user/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @AuthBinder
    public Response updateUser(@PathParam("id") int id, @FormParam("login") String login, @FormParam("nom") String nom, @FormParam("prenom") String prenom){
        Response retour = null;
        try
        {
            DecodedJWT jwt = (DecodedJWT) this.servletRequest.getAttribute("jwtData");
            int idUser = jwt.getClaim("id").asInt();
            int rank = jwt.getClaim("rank").asInt();
            
            if(id == idUser || rank >= 50){
                User theUser = new User(id , login, nom, prenom, "");
                UDAO.update(theUser);
                retour = Response.ok(createObjectBuilder().add("success", true).build()).build();
            }
            else{
                throw new Exception("Access Denied");//TODO change it
            }
        }
        catch(Exception e)
        {
            retour = Response.status(500).entity(createObjectBuilder().add("success", false).add("error", e.getMessage()).build()).build();
        }
        return retour;
    }
    
    /**
     * Suppresion d'un utilisateur
     * @param id
     * @return Response Object
     * @author Manu
     */
    @DELETE
    @Path("user/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @AuthBinder
    public Response delUser(@PathParam("id") int id){
        Response retour = null;
        try
        {
            DecodedJWT jwt = (DecodedJWT) this.servletRequest.getAttribute("jwtData");
            int rank = jwt.getClaim("rank").asInt();
            
            if(rank >= 50){
                UDAO.deletebyid(id);
                retour = Response.ok(createObjectBuilder().add("success", true).build()).build();
            }
            else{
                throw new Exception("Access Denied");//TODO change it
            }
        }
        catch(Exception e)
        {
            retour = Response.status(500).entity(createObjectBuilder().add("success", false).add("error", e.getMessage()).build()).build();
        }
        return retour;
    }
    
    /**
     * Set password
     * @param id
     * @param pass
     * @return Response Object
     * @author Manu
     */
    @POST
    @Path("user/{id}/pass")
    @Produces(MediaType.APPLICATION_JSON)
    @AuthBinder
    public Response setUserPass(@PathParam("id") int id, @FormParam("pass") String pass, @FormParam("oldpass") String oldpass){
        Response retour = null; 
        try 
        { 
            String oldhashdb = UDAO.getHashById(id); 
            if(UserUtils.checkPass(oldpass, oldhashdb)){ 
                if(UserUtils.setPass(pass, id)){ 
                    retour = Response.ok(createObjectBuilder().add("success", true).build()).build(); 
                } 
                else{ 
                    retour = Response.status(500).entity(createObjectBuilder().add("success", false).add("error", "xxxa").build()).build(); 
                } 
            } 
            else{ 
                retour = Response.status(500).entity(createObjectBuilder().add("success", false).add("error", "xxxz").build()).build(); 
            } 
        } 
        catch(Exception e)
        {
            retour = Response.status(500).entity(createObjectBuilder().add("success", false).add("error", e.getMessage()).build()).build();
        }
        return retour;
    }
}
