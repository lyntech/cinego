package com.lyntech.cinegoapi.Routes;

import com.lyntech.cinegoapi.DAO.CinemaDAO;
import com.lyntech.cinegoapi.Model.Films;
import com.lyntech.cinegoapi.Utils.WS.MovieDB;
import com.lyntech.cinegoapi.DAO.SeanceDAO;
import com.lyntech.cinegoapi.Model.Cinema;
import com.lyntech.cinegoapi.Model.FilmsCasts;
import com.lyntech.cinegoapi.Model.FilmsVideos;
import com.lyntech.cinegoapi.Model.Seance;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import static javax.json.Json.createObjectBuilder;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * Routes d'accès aux infos des films
 * @author manug
 */
@Path("/films")
public class FilmsRoutes {
    @Context
    private UriInfo context;
    
    /**
     *  Recherche de films par nom
     * @param query
     * @return Response Object
     * @author Manu
     */
    @GET
    @Path("/search/{query}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchFilms(@PathParam("query") String query){
        MovieDB mdb = new MovieDB();
        JsonArrayBuilder JsonABuilder = Json.createArrayBuilder();
        try {
            for(Films unfilm : mdb.searchFilms(query)){
                JsonObject jsonfilm = createObjectBuilder().add("id", unfilm.getId()).add("title", unfilm.getTitle()).add("date", unfilm.getDate()).add("poster", unfilm.getPoster()).build();
                JsonABuilder.add(jsonfilm);
            }
        } catch (IOException ex) {
            Logger.getLogger(FilmsRoutes.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        return Response.status(Response.Status.OK).entity(createObjectBuilder().add("success", true).add("content",JsonABuilder.build()).build()).build();
    }
    
    /**
     * Recuperation d'infos d'un film avec son id
     * @param id
     * @return Response Object
     * @author Manu
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFilmsInfos(@PathParam("id") int id){
        MovieDB mdb = new MovieDB();
        JsonArrayBuilder JsonABuilder = Json.createArrayBuilder();
        try {
            Films unfilm = mdb.getFilmsInfos(id);
            JsonObject jsonfilm = createObjectBuilder().add("id", unfilm.getId()).add("title", unfilm.getTitle()).add("date", unfilm.getDate()).add("poster", unfilm.getPoster()).add("overview", unfilm.getSummary()).build();
            JsonABuilder.add(jsonfilm);
        } catch (IOException ex) {
            Logger.getLogger(FilmsRoutes.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        return Response.status(Response.Status.OK).entity(createObjectBuilder().add("success", true).add("content",JsonABuilder.build()).build()).build();
    }
    
    /**
     * Récupération des films programmé sur toutes séances confondues
     * @return Response Object
     * @author Manu
     */
    @GET
    @Path("/seances")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFilmsBySeances(){
        SeanceDAO SDAO = new SeanceDAO();
        MovieDB mdb = new MovieDB();
        JsonArrayBuilder JsonABuilder = Json.createArrayBuilder();
        try {
            List<Integer> filmsList = SDAO.getFilmsSeances();
            for(int filmId : filmsList){
                System.out.println(filmId);
                Films unfilm = mdb.getFilmsInfos(filmId);
                JsonObject jsonfilm = createObjectBuilder().add("id", unfilm.getId()).add("title", unfilm.getTitle()).add("date", unfilm.getDate()).add("poster", unfilm.getPoster()).add("overview", unfilm.getSummary()).build();
                JsonABuilder.add(jsonfilm);
            }
        } catch (IOException ex) {
            Logger.getLogger(FilmsRoutes.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        return Response.status(Response.Status.OK).entity(createObjectBuilder().add("success", true).add("content",JsonABuilder.build()).build()).build();
    }
    
    /**
     * Récupération des  séances d'un films
     * @return Response Object
     * @author Manu
     */
    @GET
    @Path("{id}/seances")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFutureSeances(@PathParam("id") int idFilm){
        SeanceDAO SDAO = new SeanceDAO();
        MovieDB mdb = new MovieDB();
        CinemaDAO CDAO = new CinemaDAO();
        
        JsonArrayBuilder JsonABuilder = Json.createArrayBuilder();
        Films unfilm = null;
        try {
            unfilm = mdb.getFilmsInfos(idFilm);
            List<Cinema> cinemaList = CDAO.getCinemaLinkedToFilm(idFilm);
            
            for(Cinema cinema : cinemaList){
                JsonArrayBuilder tmpJsonBuilder = Json.createArrayBuilder();
                JsonObjectBuilder cineJson = cinema.toJsonObjectLight();
                //JsonABuilder.add(cinema.toJsonObjectLight());
                //System.out.println();
                List<Seance> seanceList = SDAO.getFutureSeancesbyFilmsByCine(idFilm, cinema.getIdCinema());
                for(Seance seance : seanceList){
                    tmpJsonBuilder.add(seance.toJsonObjectLight());
                }
                cineJson.add("seances", tmpJsonBuilder.build());
                JsonABuilder.add(cineJson);
            }
            
        } catch (IOException ex) {
            Logger.getLogger(FilmsRoutes.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        return Response.status(Response.Status.OK).entity(createObjectBuilder().add("success", true).add("content",createObjectBuilder().add("film", unfilm.toJsonObject()).add("seancesbycinema", JsonABuilder.build()).build()).build()).build();
    }
    
    @GET
    @Path("{id}/videos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFilmsVideos(@PathParam("id") int id){
        MovieDB mdb = new MovieDB();
        JsonArrayBuilder JsonABuilder = Json.createArrayBuilder();
        try {
            for(FilmsVideos unFilmVideos : mdb.getFilmsVideos(id)){
                JsonObject jsonFilmVideos = createObjectBuilder().add("id", unFilmVideos.getId()).add("key", unFilmVideos.getKey()).add("name", unFilmVideos.getName()).add("type", unFilmVideos.getType()).build();
                JsonABuilder.add(jsonFilmVideos);
            }
        } catch (IOException ex) {
            Logger.getLogger(FilmsRoutes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(Response.Status.OK).entity(createObjectBuilder().add("success", true).add("content",JsonABuilder.build()).build()).build();
    }
    
    @GET
    @Path("{id}/credits")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFilmsCasts(@PathParam("id") int id){
        MovieDB mdb = new MovieDB();
        JsonArrayBuilder JsonABuilder = Json.createArrayBuilder();
        try {
            for(FilmsCasts unFilmCasts : mdb.getFilmsCasts(id)){
                JsonObject jsonFilmCasts = createObjectBuilder().add("cast_id", unFilmCasts.getCast_id()).add("character", unFilmCasts.getCharacter()).add("id", unFilmCasts.getId()).add("name", unFilmCasts.getName()).add("profile_path", unFilmCasts.getProfile_path()).build();
                JsonABuilder.add(jsonFilmCasts);
            }
        } catch (IOException ex) {
            Logger.getLogger(FilmsRoutes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(Response.Status.OK).entity(createObjectBuilder().add("success", true).add("content",JsonABuilder.build()).build()).build();
    }
}
