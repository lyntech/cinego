package com.lyntech.cinegoapi;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 * Class de configuration
 * @author manug
 */
@javax.ws.rs.ApplicationPath("v1")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.lyntech.cinegoapi.Main.class);
        resources.add(com.lyntech.cinegoapi.Routes.AuthRoutes.class);
        resources.add(com.lyntech.cinegoapi.Routes.CinemaRoutes.class);
        resources.add(com.lyntech.cinegoapi.Routes.EnseigneRoutes.class);
        resources.add(com.lyntech.cinegoapi.Routes.FilmsRoutes.class);
        resources.add(com.lyntech.cinegoapi.Routes.ReservationRoute.class);
        resources.add(com.lyntech.cinegoapi.Routes.SalleRoute.class);
        resources.add(com.lyntech.cinegoapi.Routes.SeanceRoutes.class);
        resources.add(com.lyntech.cinegoapi.Routes.TarifRoutes.class);
        resources.add(com.lyntech.cinegoapi.Routes.TechnoRoute.class);
        resources.add(com.lyntech.cinegoapi.Routes.UserRoutes.class);
        resources.add(com.lyntech.cinegoapi.Utils.Filter.AuthFilter.class);
        resources.add(com.lyntech.cinegoapi.Utils.Filter.CORSFilter.class);
    }
    
}
