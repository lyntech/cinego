package com.lyntech.cinegoapi;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.lyntech.cinegoapi.Utils.MailSender;
import com.lyntech.cinegoapi.Utils.UserUtils;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * REST Web Service
 *
 * @author manug
 */
@Path("main")
public class Main {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Main
     */
    public Main() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response Test(){

        /*UserDAO test = new UserDAO();
        User user = test.get(1);
        System.out.println(user.getNom());
        System.out.println(UserUtils.hashPass("test"));
        Algorithm algorithm = Algorithm.HMAC256("secret");
        String token = JWT.create().withIssuer("auth0").sign(algorithm);*/
        
        MailSender sender = new MailSender();
        
        sender.sendMail("manu.guihur@gmail.com", 0, 1);
        
        return Response.status(Status.OK).entity("ok").build();
    }
}
