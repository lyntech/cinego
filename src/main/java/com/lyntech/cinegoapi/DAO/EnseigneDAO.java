/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.DAO;

import com.lyntech.cinegoapi.Model.Cinema;
import com.lyntech.cinegoapi.Model.Enseigne;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author manug
 */
public class EnseigneDAO extends DAO<Enseigne> {
    
    /**
     *
     */
    public EnseigneDAO()
    {
        
    }
    
    /**
     *
     * @param id
     * @return
     */
    @Override
    public Enseigne get(int id) {
        Enseigne uneEnseigne = null;
        CinemaDAO CDAO = new CinemaDAO();
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM enseigne WHERE idEnseigne ="+id);
            if(rset.next())
            {
                uneEnseigne = new Enseigne(rset.getInt("idEnseigne"), rset.getString("nom"),CDAO.getCinemaEnseigne(rset.getInt("idEnseigne")));
            }
            return uneEnseigne;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @return
     */
    @Override
    public List<Enseigne> getAll() {
        List<Enseigne> ListeEnseigne = new ArrayList<>();
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM Enseigne");
            while(rset.next())
            {
                ListeEnseigne.add(this.get(rset.getInt("idEnseigne")));
            }
            return ListeEnseigne;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @param newEnseigne
     * @return
     */
    @Override
    public int create(Enseigne newEnseigne) {
        try 
        {
            String[] returnID = {"idEnseigne"};
            String query = " insert into enseigne (idEnseigne, nom) values (null,?)";
            PreparedStatement preparestmt = this.connect.prepareStatement(query,returnID);
            preparestmt.setString(1,newEnseigne.getNom());
            preparestmt.executeUpdate();
            ResultSet RS = preparestmt.getGeneratedKeys();
            if(RS.next())
            {
                return RS.getInt(1);
            }
            else
            {
                return 0;
            }
        }
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @param modifEnseigne
     */
    @Override
    public void update(Enseigne modifEnseigne) {
        try 
        {
            String query = "update enseigne set nom = ? where idEnseigne = ?";
            PreparedStatement preparedStmt = this.connect.prepareStatement(query);
            preparedStmt.setString(1, modifEnseigne.getNom());
            preparedStmt.setInt(2,modifEnseigne.getIdEnseigne());
            preparedStmt.executeUpdate();
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    /**
     *
     * @param id
     */
    @Override
    public void updatebyid(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param t
     */
    @Override
    public void updateMultiple(List<Enseigne> t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param id
     */
    @Override
    public void updateMultiplebyid(List<Integer> id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param supprEnseigne
     */
    @Override
    public void delete(Enseigne supprEnseigne) {
        try 
        {
            String query = "DELETE FROM enseigne WHERE idEnseigne = ?";
            PreparedStatement preparestmt = this.connect.prepareStatement(query);
            preparestmt.setInt(1,supprEnseigne.getIdEnseigne());
            preparestmt.execute();
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @param IDSupprEnseigne
     */
    @Override
    public void deletebyid(int IDSupprEnseigne) {
        try 
        {
            String query = " DELETE FROM enseigne WHERE idEnseigne = ?";
            PreparedStatement preparestmt = this.connect.prepareStatement(query);
            preparestmt.setInt(1,IDSupprEnseigne);
            preparestmt.execute();
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
