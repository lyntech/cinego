/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.DAO;

import com.lyntech.cinegoapi.Model.PlaceReserver;
import com.lyntech.cinegoapi.Model.Reservation;
import com.lyntech.cinegoapi.Model.Seance;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Yoann
 */
public class ReservationDAO extends DAO<Reservation> {
    static SeanceDAO SeDAO;
    static UserDAO UDAO;
    
    @Override
    public Reservation get(int id) {
        Reservation uneReservation = null;
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM reservation WHERE idReservation ="+id);
            if(rset.next())
            {
                uneReservation = new Reservation(rset.getInt("idReservation"), rset.getInt("idUtilisateur"), rset.getString("date"),rset.getInt("idSeance"));
            }
            return uneReservation;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public List<Reservation> getAll() {
        List<Reservation> ListeReservation = new ArrayList<>();
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM reservation");
            while(rset.next())
            {
                ListeReservation.add(new Reservation(rset.getInt("idReservation"), rset.getInt("idUtilisateur"),rset.getString("date"),rset.getInt("idSeance")));
            }
            return ListeReservation;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    public List<Reservation> getReservationsByUser(int idUser) {
        List<Reservation> ListeReservation = new ArrayList<>();
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM reservation WHERE idUtilisateur = "+idUser);
            while(rset.next())
            {
                ListeReservation.add(new Reservation(rset.getInt("idReservation"), rset.getInt("idUtilisateur"),rset.getString("date"),rset.getInt("idSeance")));
            }
            return ListeReservation;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    public List<Reservation> getSeanceReservations(int idSeance) {
        List<Reservation> ListeReservation = new ArrayList<>();
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM reservation WHERE idSeance = "+idSeance);
            while(rset.next())
            {
                ListeReservation.add(new Reservation(rset.getInt("idReservation"), rset.getInt("idUtilisateur"), rset.getString("date"),rset.getInt("idSeance")));
            }
            return ListeReservation;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    public List<PlaceReserver> getPlacesReservation(int idReservation) {
        List<PlaceReserver> ListePlaces = new ArrayList<>();
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM placeReserver WHERE idReservation = "+idReservation);
            while(rset.next())
            {
                ListePlaces.add(new PlaceReserver(rset.getInt("idPlace"), rset.getInt("idReservation"),rset.getDouble("prix")));
            }
            return ListePlaces;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    
    @Override
    public int create(Reservation newReservation) {
        try 
        {
            String[] returnID = {"idReservation"};
            String query = "insert into reservation (idReservation, idUtilisateur, date, idSeance) values (null, ?, ?, ?)";
            PreparedStatement preparestmtReservation = this.connect.prepareStatement(query,returnID);
            preparestmtReservation.setInt(1,newReservation.getIdUtilisateur());
            preparestmtReservation.setString(2,newReservation.getDate());
            preparestmtReservation.setInt(3,newReservation.getIdSeance());
            preparestmtReservation.executeUpdate();
            ResultSet RS = preparestmtReservation.getGeneratedKeys();
            if(RS.next())
            {
                return RS.getInt(1);
            }
            else
            {
                return 0;
            }
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    public int createPlace(PlaceReserver newPlaceReserver) {
        try
        {
            String[] returnID_bis = {"idPlace"};
            String query2 = "insert into placereserver (idPlace, idReservation, prix) values (?, ?, ?)";
            PreparedStatement preparestmtPlaceReserve = this.connect.prepareStatement(query2,returnID_bis);
            preparestmtPlaceReserve.setInt(1,newPlaceReserver.getIdPlace());
            preparestmtPlaceReserve.setInt(2,newPlaceReserver.getIdReservation());
            preparestmtPlaceReserve.setDouble(3,newPlaceReserver.getPrix());
            preparestmtPlaceReserve.executeUpdate();
            ResultSet RS = preparestmtPlaceReserve.getGeneratedKeys();
            if(RS.next())
            {
                return RS.getInt(1);
            }
            else
            {
                return 0;
            }
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public void update(Reservation modifReservation) {
        try 
        {
            String query = "update reservation set idUtilisateur = ?,date = ?, idSeance = ? where idReservation = ?";
            PreparedStatement preparedStmt = this.connect.prepareStatement(query);
            preparedStmt.setInt(1, modifReservation.getIdUtilisateur());
            preparedStmt.setString(2,modifReservation.getDate());
            preparedStmt.setInt(3,modifReservation.getIdSeance());
            preparedStmt.executeUpdate();
        } 
        catch (NumberFormatException | SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public void updatebyid(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateMultiple(List<Reservation> lesReservations) {
        lesReservations.forEach((uneReservation) -> {
            update(uneReservation);
        });
    }

    @Override
    public void updateMultiplebyid(List<Integer> id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void delete(Reservation supprReservation) {
        try 
        {
            String query = "DELETE FROM reservation WHERE idReservation = ?";
            PreparedStatement preparestmt = this.connect.prepareStatement(query);
            preparestmt.setInt(1,supprReservation.getIdReservation());
            preparestmt.execute();
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public void deletebyid(int IdSupprReservation) {
        try 
        {
            String query = " DELETE FROM reservation WHERE idReservation = ?";
            PreparedStatement preparestmt = this.connect.prepareStatement(query);
            preparestmt.setInt(1,IdSupprReservation);
            preparestmt.execute();
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
