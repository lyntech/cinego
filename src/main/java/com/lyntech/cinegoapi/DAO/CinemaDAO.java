/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.DAO;

import com.lyntech.cinegoapi.Model.Cinema;
import com.lyntech.cinegoapi.Model.Enseigne;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CinemaDAO extends DAO<Cinema> {

    static TarifDAO TDAO = new TarifDAO();
    static SalleDAO SDAO = new SalleDAO();
    public CinemaDAO()
    {
        if(TarifDAO.CDAO == null)//Singleton CDAO
        {
            TarifDAO.CDAO = this;
        }
    }
    
    /**
     *
     * @param id
     * @return
     */
    @Override
    public Cinema get(int id) {
        Cinema unCinema = null;
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM cinema WHERE idCinema ="+id);
            if(rset.next())
            {
                unCinema = new Cinema(rset.getInt("idCinema"), rset.getString("nom"),rset.getString("adresse"),rset.getInt("idEnseigne"));
            }
            return unCinema;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @return
     */
    @Override
    public List<Cinema> getAll() {
        List<Cinema> ListeCinema = new ArrayList<>();
        try
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM Cinema");
            while(rset.next())
            {
                ListeCinema.add(new Cinema(rset.getInt("idCinema"), rset.getString("nom"),rset.getString("adresse"),rset.getInt("idEnseigne")));
            }
            return ListeCinema;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    /**
     *
     * @param IDEnseigne
     * @return
     */
    public List<Cinema> getCinemaEnseigne(int IDEnseigne) {
        EnseigneDAO EDAO = new EnseigneDAO();
        TarifDAO TDAO = new TarifDAO();
        List<Cinema> ListeCinema = new ArrayList<>();
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM Cinema WHERE idEnseigne ="+IDEnseigne);
            while(rset.next())
            {
                ListeCinema.add(new Cinema(rset.getInt("idCinema"), rset.getString("nom"),rset.getString("adresse"),IDEnseigne,TDAO.getCinemaTarifs(rset.getInt("idCinema")),SDAO.getSalleCinema(rset.getInt("idCinema"))));
            }
            return ListeCinema;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @param newCinema
     * @return
     */
    @Override
    public int create(Cinema newCinema) {
        try 
        {
            String[] returnID = {"idCinema"};
            String query = " insert into cinema (idCinema, nom, adresse,idEnseigne) values (null,?, ?, ?)";
            PreparedStatement preparestmt = this.connect.prepareStatement(query,returnID);
            preparestmt.setString(1,newCinema.getNom());
            preparestmt.setString(2,newCinema.getAdresse());
            preparestmt.setInt(3,newCinema.getidEnseigne());
            preparestmt.executeUpdate();
            ResultSet RS = preparestmt.getGeneratedKeys();
            if(RS.next())
            {
                return RS.getInt(1);
            }
            else
            {
                return 0;
            }
        }
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @param modifCinema
     */
    @Override
    public void update(Cinema modifCinema) {
        try 
        {
            String query = "update cinema set nom = ?,adresse = ?, idEnseigne = ? where idCinema = ?";
            PreparedStatement preparedStmt = this.connect.prepareStatement(query);
            preparedStmt.setString(1, modifCinema.getNom());
            preparedStmt.setString(2,modifCinema.getAdresse());
            preparedStmt.setInt(3,modifCinema.getidEnseigne());
            preparedStmt.setInt(4,modifCinema.getIdCinema());
            preparedStmt.executeUpdate();
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @param IDSupprCinema
     */
    @Override
    public void updatebyid(int IDSupprCinema) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param t
     */
    @Override
    public void updateMultiple(List<Cinema> t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param id
     */
    @Override
    public void updateMultiplebyid(List<Integer> id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     *
     * @param supprCinema
     */
    @Override
    public void delete(Cinema supprCinema) 
    {
        try 
        {
            String query = " DELETE FROM cinema WHERE idCinema = ?";
            PreparedStatement preparestmt = this.connect.prepareStatement(query);
            preparestmt.setInt(1,supprCinema.getIdCinema());
            preparestmt.execute();
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @param IDSupprCinema
     */
    @Override
    public void deletebyid(int IDSupprCinema) 
    {
        try 
        {
            String query = "DELETE FROM cinema WHERE idCinema = ?";
            PreparedStatement preparestmt = this.connect.prepareStatement(query);
            preparestmt.setInt(1,IDSupprCinema);
            preparestmt.execute();
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    public List<Cinema> getCinemaLinkedToFilm(int idFilm) {
        List<Cinema> ListeCinema = new ArrayList<>();
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT cinema.idCinema, cinema.nom, cinema.adresse FROM seance, salle, cinema WHERE seance.idSalle = salle.idSalle AND salle.idCinema = cinema.idCinema AND seance.idFilm = "+idFilm+" AND seance.date >= CURDATE() GROUP BY cinema.idCinema");
            while(rset.next())
            {
                ListeCinema.add(new Cinema(rset.getInt("idCinema"), rset.getString("nom"),rset.getString("adresse"), 0));
            }
            return ListeCinema;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
}
