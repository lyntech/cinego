/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.DAO;

import com.lyntech.cinegoapi.Model.Salle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 * @author TERUEL
 */
public class SalleDAO extends DAO<Salle> {

    // static CinemaDAO CDAO;
    static TechnoDAO TechDAO;

    /**
     *
     */
    public SalleDAO()
    {
        /*if(CinemaDAO.TDAO == null)
        {
            CinemaDAO.TDAO = this;
        }*/
        if(TechnoDAO.SDAO == null)
        {
            TechnoDAO.SDAO = this;
        }
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public Salle get(int id) {
        Salle salle = null;
        try {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM salle WHERE idSalle = " + id);
            if (rset.next()) {
                salle = new Salle(id, rset.getString("nom"), rset.getString("plan"), rset.getInt("nbPlace"), rset.getInt("idCinema"), rset.getInt("idTechno"));
            }
            return salle;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     *
     * @return
     */
    @Override
    public List<Salle> getAll() {
        List<Salle> listSalles = new ArrayList<>();
        try {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM salle");
            while (rset.next()) {
                listSalles.add(new Salle(rset.getInt("idSalle"), rset.getString("nom"), rset.getString("plan"), rset.getInt("nbPlace"), rset.getInt("idCinema"), rset.getInt("idTechno")));
            }
            return listSalles;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    /**
     *
     * @param idCinema
     * @return
     */
    public List<Salle> getSalleCinema(int idCinema) {
        List<Salle> listSalles = new ArrayList<>();
        try {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM salle WHERE idCinema = " + idCinema);
            while (rset.next()) {
                listSalles.add(new Salle(rset.getInt("idSalle"), rset.getString("nom"), rset.getString("plan"), rset.getInt("nbPlace"), rset.getInt("idCinema"), rset.getInt("idTechno")));
            }
            return listSalles;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @param idTechno
     * @return
     */
    public List<Salle> getTechnoSalles(int idTechno) {
        List<Salle> ListeSalle = new ArrayList<>();
        try
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM salle WHERE idTechno = "+idTechno);
            while(rset.next())
            {
                ListeSalle.add(new Salle(rset.getInt("idSalle"), rset.getString("nom"), rset.getString("plan"), rset.getInt("nbPlace"), rset.getInt("idCinema"), rset.getInt("idTechno")));
            }
            return ListeSalle;
        }
        catch (SQLException e)
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @param salle
     * @return
     */
    @Override
    public int create(Salle salle) {
        try {
            String[] returnID = {"idSalle"};
            String query = "INSERT INTO salle (nom, plan, nbPlace, idCinema, idTechno) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement prepareStmt = this.connect.prepareStatement(query, returnID);
            prepareStmt.setString(1, salle.getNom());
            prepareStmt.setString(2, salle.getPlan());
            prepareStmt.setInt(3, salle.getNbPlace());
            prepareStmt.setInt(4, salle.getIdCinema());
            prepareStmt.setInt(5, salle.getIdTechno());
            prepareStmt.executeUpdate();
            ResultSet RS = prepareStmt.getGeneratedKeys();
            if(RS.next())
            {
                return RS.getInt(1);
            }
            else
            {
                return 0;
            }
        } catch (Exception e) {
            throw new UnsupportedOperationException(e.getMessage());
        }
    }

    /**
     *
     * @param salle
     */
    @Override
    public void update(Salle salle) {
        try {
            String query = "UPDATE salle SET nom = ?,plan = ?, nbPlace = ?, idCinema = ?, idTechno = ? WHERE idSalle = ?";
            PreparedStatement preparedStmt = this.connect.prepareStatement(query);
            preparedStmt.setString(1, salle.getNom());
            preparedStmt.setString(2, salle.getPlan());
            preparedStmt.setInt(3, salle.getNbPlace());
            preparedStmt.setInt(4, salle.getIdCinema());
            preparedStmt.setInt(5, salle.getIdTechno());
            preparedStmt.setInt(6, salle.getIdSalle());
            preparedStmt.executeUpdate();
        } catch (Exception e) {
            throw new UnsupportedOperationException(e.getMessage());
        }
    }

    /**
     *
     * @param id
     */
    @Override
    public void updatebyid(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param lesSalles
     */
    @Override
    public void updateMultiple(List<Salle> lesSalles) {
        lesSalles.forEach((uneSalle) -> {
            update(uneSalle);
        });
    }

    /**
     *
     * @param id
     */
    @Override
    public void updateMultiplebyid(List<Integer> id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param supprSalle
     */
    @Override
    public void delete(Salle supprSalle) {
        try {
            String query = "DELETE FROM salle WHERE idSalle = ?";
            PreparedStatement pstmt = this.connect.prepareStatement(query);
            pstmt.setInt(1, supprSalle.getIdSalle());
            pstmt.execute();

        } catch (SQLException e) {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @param idSupprSalle
     */
    @Override
    public void deletebyid(int idSupprSalle) {
        try {
            String query = " DELETE FROM salle WHERE idSalle = ?";
            PreparedStatement preparestmt = this.connect.prepareStatement(query);
            preparestmt.setInt(1, idSupprSalle);
            preparestmt.execute();
        } catch (SQLException e) {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    public void updatePlan(String plan, int id) {
        try {                        
            PreparedStatement preparestmt = this.connect.prepareStatement("UPDATE salle SET plan = ? WHERE idSalle = ?");
            preparestmt.setString(1, plan);
            preparestmt.setInt(2,id);
            preparestmt.execute();
        } catch (Exception e) {
            
        }
    }
}
