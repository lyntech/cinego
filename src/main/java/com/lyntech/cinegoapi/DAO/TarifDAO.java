/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.DAO;

import com.lyntech.cinegoapi.Model.Tarif;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author manug
 */
public class TarifDAO extends DAO<Tarif> {

    static CinemaDAO CDAO;
    
    /**
     *
     */
    public TarifDAO()
    {
        if(CinemaDAO.TDAO == null)
        {
            CinemaDAO.TDAO = this;
        }
    }
    
    /**
     *
     * @param id
     * @return
     */
    @Override
    public Tarif get(int id) {
        Tarif unTarif = null;
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM tarif WHERE idTarif ="+id);
            if(rset.next())
            {
                unTarif = new Tarif(rset.getInt("idTarif"), rset.getString("nom"),rset.getDouble("prix"),rset.getInt("idCinema"));
            }
            return unTarif;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @return
     */
    @Override
    public List<Tarif> getAll() {
        List<Tarif> ListeTarif = new ArrayList<>();
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM tarif");
            while(rset.next())
            {
                ListeTarif.add(new Tarif(rset.getInt("idTarif"), rset.getString("nom"),rset.getDouble("prix"),rset.getInt("idCinema")));
            }
            return ListeTarif;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @param idCinema
     * @return
     */
    public List<Tarif> getCinemaTarifs(int idCinema) {
        List<Tarif> ListeTarif = new ArrayList<>();
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM tarif WHERE idCinema = "+idCinema);
            while(rset.next())
            {
                ListeTarif.add(new Tarif(rset.getInt("idTarif"), rset.getString("nom"),rset.getDouble("prix"),rset.getInt("idCinema")));
            }
            return ListeTarif;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    /**
     *
     * @param newTarif
     * @return
     */
    @Override
    public int create(Tarif newTarif) {
        try 
        {
            String[] returnID = {"idTarif"};
            String query = "insert into tarif (idTarif, nom, prix,idCinema) values (null,?, ?, ?)";
            PreparedStatement preparestmt = this.connect.prepareStatement(query,returnID);
            preparestmt.setString(1,newTarif.getNom());
            preparestmt.setDouble(2,newTarif.getPrix());
            preparestmt.setInt(3,newTarif.getIDcinema());
            preparestmt.executeUpdate();
            ResultSet RS = preparestmt.getGeneratedKeys();
            if(RS.next())
            {
                return RS.getInt(1);
            }
            else
            {
                return 0;
            }
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @param modifTarif
     */
    @Override
    public void update(Tarif modifTarif) {
        try 
        {
            String query = "update tarif set nom = ?,prix = ?, idCinema = ? where idTarif = ?";
            PreparedStatement preparedStmt = this.connect.prepareStatement(query);
            preparedStmt.setString(1, modifTarif.getNom());
            preparedStmt.setDouble(2,modifTarif.getPrix());
            preparedStmt.setInt(3,modifTarif.getIDcinema());
            preparedStmt.setInt(4,modifTarif.getIDTarif());
            preparedStmt.executeUpdate();
        } 
        catch (NumberFormatException | SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @param id
     */
    @Override
    public void updatebyid(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param lesTarifs
     */
    @Override
    public void updateMultiple(List<Tarif> lesTarifs) {
        lesTarifs.forEach((unTarif) -> {
            update(unTarif);
        });
    }

    /**
     *
     * @param id
     */
    @Override
    public void updateMultiplebyid(List<Integer> id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     *
     * @param supprTarif
     */
    @Override
    public void delete(Tarif supprTarif) {
        try 
        {
            String query = "DELETE FROM tarif WHERE idTarif = ?";
            PreparedStatement preparestmt = this.connect.prepareStatement(query);
            preparestmt.setInt(1,supprTarif.getIDTarif());
            preparestmt.execute();
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @param IDSupprTarif
     */
    @Override
    public void deletebyid(int IDSupprTarif) {
        try 
        {
            String query = " DELETE FROM tarif WHERE idTarif = ?";
            PreparedStatement preparestmt = this.connect.prepareStatement(query);
            preparestmt.setInt(1,IDSupprTarif);
            preparestmt.execute();
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
