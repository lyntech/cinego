package com.lyntech.cinegoapi.DAO;

import java.sql.Connection;
import java.util.List;

/**
 * DAO abstract class
 * @author manug
 */
public abstract class DAO<T> {
    Connection connect = ConnectionDB.getInstance();

    public abstract T get(int id);
     
    public abstract List<T> getAll();
     
    public abstract int create(T t);
     
    public abstract void update(T t);
    
    public abstract void updatebyid(int id);
    
    public abstract void updateMultiple(List<T> t);
    
    public abstract void updateMultiplebyid(List<Integer> id);
    
    public abstract void delete(T t);
    
    public abstract void deletebyid(int id);
}
