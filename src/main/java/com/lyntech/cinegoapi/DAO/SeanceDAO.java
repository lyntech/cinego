/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.DAO;

import com.lyntech.cinegoapi.Model.Seance;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Yoann
 */
public class SeanceDAO extends DAO<Seance> {
    
    static SalleDAO SDAO;
    
    public SeanceDAO(){
        if(ReservationDAO.SeDAO == null)
        {
            ReservationDAO.SeDAO = this;
        }
    }
    
    @Override
    public Seance get(int id) {
        Seance uneSeance = null;
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM seance WHERE idSeance ="+id);
            if(rset.next())
            {
                uneSeance = new Seance(rset.getInt("idSeance"), rset.getString("date"),rset.getInt("idFilm"),rset.getInt("idSalle"));
            }
            return uneSeance;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public List<Seance> getAll() {
        List<Seance> ListeSeance = new ArrayList<>();
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM seance");
            while(rset.next())
            {
                ListeSeance.add(new Seance(rset.getInt("idSeance"), rset.getString("date"),rset.getInt("idFilm"),rset.getInt("idSalle")));
            }
            return ListeSeance;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    public List<Seance> getSalleSeances(int idSalle) {
        List<Seance> ListeSeance = new ArrayList<>();
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM seance WHERE idSalle = "+idSalle);
            while(rset.next())
            {
                ListeSeance.add(new Seance(rset.getInt("idSeance"), rset.getString("date"),rset.getInt("idFilm"),rset.getInt("idSalle")));
            }
            return ListeSeance;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    public List<Integer> getFilmsSeances(){
        List<Integer> listeFilm = new ArrayList<>();
        try 
        {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT idFilm FROM seance GROUP BY idFilm");
            while(rset.next())
            {
                listeFilm.add(rset.getInt("idFilm"));
            }
            return listeFilm;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    public List<Seance> getFutureSeancesbyFilmsByCine(int idFilm, int idCine){
        
        List<Seance> listeSeance = new ArrayList<>();
        try 
        {
            Statement stmt = this.connect.createStatement();
            //ResultSet rset = stmt.executeQuery("SELECT cinema.idCinema, cinema.nom, cinema.adresse FROM seance, salle, cinema WHERE seance.idSalle = salle.idSalle AND salle.idCinema = cinema.idCinema AND seance.idFilm = "+idFilm+" GROUP BY cinema.idCinema");
            ResultSet rset = stmt.executeQuery("SELECT * FROM seance, salle WHERE seance.idSalle = salle.idSalle AND date >= CURDATE() AND idFilm = "+idFilm+" AND salle.idCinema = "+idCine);
            while(rset.next())
            {
                listeSeance.add(new Seance(rset.getInt("idSeance"), rset.getString("date"),rset.getInt("idFilm"),rset.getInt("idSalle")));
            }
            return listeSeance;
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    @Override
    public int create(Seance newSeance) {
        try 
        {
            String[] returnID = {"idSeance"};
            String query = "insert into seance (idSeance, date, idFilm, idSalle) values (null,?, ?, ?)";
            PreparedStatement preparestmt = this.connect.prepareStatement(query,returnID);
            preparestmt.setString(1,newSeance.getDate());
            preparestmt.setInt(2,newSeance.getIdFilm());
            preparestmt.setInt(3,newSeance.getIdSalle());
            preparestmt.executeUpdate();
            ResultSet RS = preparestmt.getGeneratedKeys();
            if(RS.next())
            {
                return RS.getInt(1);
            }
            else
            {
                return 0;
            }
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public void update(Seance modifSeance) {
        try 
        {
            String query = "update seance set date = ?,idFilm = ?, idSalle = ? where idSeance = ?";
            PreparedStatement preparedStmt = this.connect.prepareStatement(query);
            preparedStmt.setString(1, modifSeance.getDate());
            preparedStmt.setInt(2,modifSeance.getIdFilm());
            preparedStmt.setInt(3,modifSeance.getIdSalle());
            preparedStmt.setInt(4,modifSeance.getIdSeance());
            preparedStmt.executeUpdate();
        } 
        catch (NumberFormatException | SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public void updatebyid(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateMultiple(List<Seance> lesSeances) {
        lesSeances.forEach((unSeance) -> {
            update(unSeance);
        });
    }

    @Override
    public void updateMultiplebyid(List<Integer> id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void delete(Seance supprSeance) {
        try 
        {
            String query = "DELETE FROM seance WHERE idSeance = ?";
            PreparedStatement preparestmt = this.connect.prepareStatement(query);
            preparestmt.setInt(1,supprSeance.getIdSeance());
            preparestmt.execute();
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public void deletebyid(int IdSupprSeance) {
        try 
        {
            String query = " DELETE FROM seance WHERE idSeance = ?";
            PreparedStatement preparestmt = this.connect.prepareStatement(query);
            preparestmt.setInt(1,IdSupprSeance);
            preparestmt.execute();
        } 
        catch (SQLException e) 
        {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
