/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.DAO;

import com.lyntech.cinegoapi.Model.Techno;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 * @author TERUEL
 */
public class TechnoDAO extends DAO<Techno> {

    static SalleDAO SDAO = new SalleDAO();

    /**
     *
     */
    public TechnoDAO()
    {
        if(SalleDAO.TechDAO == null)
        {
            SalleDAO.TechDAO = this;
        }
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public Techno get(int id) {
        Techno Techno = null;
        try {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM technologie WHERE idTechno = " + id);
            if (rset.next()) {
                Techno = new Techno(id, rset.getString("nom"), SDAO.getTechnoSalles(id));
            }
            return Techno;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     *
     * @return
     */
    @Override
    public List<Techno> getAll() {
        List<Techno> ListTechnos = new ArrayList<>();
        try {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT * FROM technologie");
            while (rset.next()) {
                ListTechnos.add(new Techno(rset.getInt("idTechno"), rset.getString("nom"), SDAO.getTechnoSalles(rset.getInt("idTechno"))));
            }
            return ListTechnos;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e.getMessage()); //To change body of generated methods, choose Tools | Templates.
        }

    }

    /**
     *
     * @param newTechno
     * @return
     */
    @Override
    public int create(Techno newTechno) {
        try {
            String[] returnId = {"idTechno"};
            String query = "INSERT INTO technologie (idTechno, nom) VALUES (NULL, ?)";
            PreparedStatement prepareStmt = this.connect.prepareStatement(query, returnId);
            prepareStmt.setString(1, newTechno.getNom());
            prepareStmt.executeUpdate();
            ResultSet RS = prepareStmt.getGeneratedKeys();
            if(RS.next())
            {
                return RS.getInt(1);
            }
            else
            {
                return 0;
            }
        } catch (Exception e) {
            throw new UnsupportedOperationException(e.getMessage());
        }
    }

    /**
     *
     * @param techno
     */
    @Override
    public void update(Techno techno) {
        try {
            String query = "UPDATE technologie SET nom = ? WHERE idTechno = ?";
            PreparedStatement preparedStmt = this.connect.prepareStatement(query);
            preparedStmt.setString(1, techno.getNom());
            preparedStmt.setInt(2, techno.getIdTechno());
            preparedStmt.executeUpdate();
        } catch (Exception e) {
            throw new UnsupportedOperationException(e.getMessage());
        }
    }

    /**
     *
     * @param id
     */
    @Override
    public void updatebyid(int id) {

    }

    /**
     *
     * @param t
     */
    @Override
    public void updateMultiple(List<Techno> t) {
        t.forEach(this::update);
    }

    /**
     *
     * @param id
     */
    @Override
    public void updateMultiplebyid(List<Integer> id) {

    }

    /**
     *
     * @param techno
     */
    @Override
    public void delete(Techno techno) {
        try {
            String query = "DELETE FROM technologie WHERE idTechno = ?";
            PreparedStatement preparedStmt = this.connect.prepareStatement(query);
            preparedStmt.setInt(1, techno.getIdTechno());
            preparedStmt.execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException(e.getMessage());
        }
    }

    /**
     *
     * @param id
     */
    @Override
    public void deletebyid(int id) {
        try {
            String query = "DELETE FROM technologie WHERE idTechno = " + id;
            PreparedStatement preparedStmt = this.connect.prepareStatement(query);
            preparedStmt.execute();
        } catch (Exception e) {
            throw new UnsupportedOperationException(e.getMessage());
        }
    }
}
