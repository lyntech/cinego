package com.lyntech.cinegoapi.DAO;

import com.lyntech.cinegoapi.Model.User;
import com.sun.tools.javac.util.Pair;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * User DAO
 * @author manug
 */
public class UserDAO extends DAO<User> {
    
    public UserDAO(){
        if(ReservationDAO.UDAO == null)
        {
            ReservationDAO.UDAO = this;
        }
    }

    @Override
    public User get(int id) {
        User user = null;
        try {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT login, nom, prenom, grade FROM utilisateur WHERE idUtilisateur ="+id);
            if(rset.next()){
                user = new User(id, rset.getString("login"), rset.getString("nom"), rset.getString("prenom"), "",rset.getInt("grade"));
            }
        } catch (Exception e) {
        }
        
        return user;
    }

    @Override
    public List<User> getAll() {
        List<User> users = new ArrayList<>();
        try {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT idUtilisateur, login, nom, prenom, grade FROM utilisateur");
            while(rset.next()){               
                users.add(new User(rset.getInt("idUtilisateur"), rset.getString("login"), rset.getString("nom"), rset.getString("prenom"), "",rset.getInt("grade")));
            }
        } catch (Exception e) {
            System.out.println("com.lyntech.cinegoapi.DAO.UserDAO.getAll()");
            System.out.println(e.toString());
        }
        
        return users;
    }

    @Override
    public int create(User t) {
        Integer retour = null;
        try {
            String[] returnID = {"idUtilisateur"};
            PreparedStatement preparestmt = this.connect.prepareStatement("INSERT INTO utilisateur (login, nom, prenom, pass, grade) VALUES(?, ?, ?, ?, ?)", returnID);
            preparestmt.setString(1,t.getLogin());
            preparestmt.setString(2,t.getNom());
            preparestmt.setString(3,t.getPrenom());
            preparestmt.setString(4,t.getPass());
            preparestmt.setInt(5,t.getRank());
            preparestmt.execute();
            
            ResultSet RS = preparestmt.getGeneratedKeys();
            if(RS.next())
            {
                retour = RS.getInt(1);
            }
            else
            {
                retour = 0;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
        return retour;
    }

    @Override
    public void update(User t) {
        try {
            PreparedStatement preparestmt = this.connect.prepareStatement("UPDATE utilisateur SET login = ?, nom = ?, prenom = ?");
            preparestmt.setString(1,t.getLogin());
            preparestmt.setString(2,t.getNom());
            preparestmt.setString(3,t.getPrenom());
            preparestmt.execute();
        } catch (Exception e) {
            
        }
    }

    @Override
    public void delete(User t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updatebyid(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateMultiple(List<User> t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateMultiplebyid(List<Integer> id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deletebyid(int id) {
        try {
            PreparedStatement preparestmt = this.connect.prepareStatement("DELETE FROM utilisateur WHERE idUtilisateur = ?");
            preparestmt.setInt(1,id);
            preparestmt.execute();
        } catch (Exception e) {
            
        }
    }

    public void updatePass(String hash, int idUser) {
        try {
            PreparedStatement preparestmt = this.connect.prepareStatement("UPDATE utilisateur SET pass = ? WHERE idUtilisateur = ?");
            preparestmt.setString(1,hash);
            preparestmt.setInt(2,idUser);
            preparestmt.execute();
        } catch (Exception e) {
            
        }
    }
    
    public String getHashById(int id) {
        String hash = null;
        try {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT pass FROM utilisateur WHERE idUtilisateur ="+id);
            if(rset.next()){
                hash = rset.getString("pass");
            }
        } catch (Exception e) {
        }
        
        return hash;
    }
    
    public Pair getUserbyLogin(String login) {
        Pair retour = new Pair<>(false, new User());
        try {
            Statement stmt = this.connect.createStatement();
            ResultSet rset = stmt.executeQuery("SELECT idUtilisateur, login, nom, prenom, grade, pass FROM utilisateur WHERE login = '"+login+"'");
            if(rset.next()){
                retour = new Pair<>(true, new User(rset.getInt("idUtilisateur"), rset.getString("login"), rset.getString("nom"), rset.getString("prenom"), rset.getString("pass"),rset.getInt("grade")));
            }
        } catch (Exception e) {
        }
        
        return retour;
    }
}
