package com.lyntech.cinegoapi.DAO;

import com.lyntech.cinegoapi.Utils.ConfigReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class de connection a la db
 * @author manug
 */
public class ConnectionDB {
    
    //public static final String USER = "root", PASS = "passantho", DRIVER_CLASS = "jdbc:mysql://localhost:3306/cinego?serverTimezone=UTC";
    
    private static Connection connect;
    
    static Connection getInstance(){
        if(connect == null){
            try {
                ConfigReader config = new ConfigReader();
                connect = DriverManager.getConnection(config.getConfig("db.driver"), config.getConfig("db.user"), config.getConfig("db.pass"));
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IOException ex) {
                Logger.getLogger(ConnectionDB.class.getName()).log(Level.SEVERE, null, ex);
            }
        }		
        return connect;
    }
}
