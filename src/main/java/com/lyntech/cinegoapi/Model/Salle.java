/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Model;

import com.lyntech.cinegoapi.DAO.CinemaDAO;
import com.lyntech.cinegoapi.DAO.TechnoDAO;
import java.io.IOException;
import javax.json.JsonObject;

import java.util.List;

import static javax.json.Json.createObjectBuilder;

/**
 *
 * @author TERUEL
 */
public class Salle {
    private int idSalle;
    private String nom;
    private String plan;
    private int nbPlace;
    private int idCinema;
    private int idTechno;
    private List<Seance> LesSeances;

    
    public Salle(){

    }

    public Salle(String nom, String plan, int nbPlace, int idCinema, int idTechno) {
        this.nom = nom;
        this.plan = plan;
        this.nbPlace = nbPlace;
        this.idCinema = idCinema;
        this.idTechno = idTechno;
    }

    public Salle(int idSalle, String nom, String plan, int nbPlace, int idCinema, int idTechno) {
        this.idSalle = idSalle;
        this.nom = nom;
        this.plan = plan;
        this.nbPlace = nbPlace;
        this.idCinema = idCinema;
        this.idTechno = idTechno;
    }

    /*public Salle(int idSalle, String nom, String plan, int nbPlace, int idCinema, int idTechno, List<Seance> lesSeances) {
        this.idSalle = idSalle;
        this.nom = nom;
        this.plan = plan;
        this.nbPlace = nbPlace;
        this.idCinema = idCinema;
        this.idTechno = idTechno;
        // this.LesSeances = lesSeances;
    }*/

    /**
     * @return the idSalle
     */
    public int getIdSalle() {
        return idSalle;
    }

    /**
     * @param idSalle the idSalle to set
     */
    public void setIdSalle(int idSalle) {
        this.idSalle = idSalle;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the plan
     */
    public String getPlan() {
        return plan;
    }

    /**
     * @param plan the plan to set
     */
    public void setPlan(String plan) {
        this.plan = plan;
    }

    /**
     * @return the nbPlace
     */
    public int getNbPlace() {
        return nbPlace;
    }

    /**
     * @param nbPlace the nbPlace to set
     */
    public void setNbPlace(int nbPlace) {
        this.nbPlace = nbPlace;
    }

    public int getIdCinema() {
        return idCinema;
    }

    public void setIdCinema(int idCinema) {
        this.idCinema = idCinema;
    }

    public int getIdTechno() {
        return idTechno;
    }

    public void setIdTechno(int idTechno) {
        this.idTechno = idTechno;
    }
public List<Seance> getLesSeances() {
        return LesSeances;
    }

    public void setLesSeances(List<Seance> LesSeances) {
        this.LesSeances = LesSeances;
    }


    public JsonObject toJsonObject() throws IOException
    {
        CinemaDAO CDAO = new CinemaDAO();
        TechnoDAO TechDAO = new TechnoDAO();
        return createObjectBuilder()
                .add("id", this.idSalle)
                .add("name", this.nom)
                .add("blueprint", this.plan)
                .add("placelenght", this.nbPlace)
                .add("cinema",CDAO.get(idCinema).toJsonObject())
                .add("techno",TechDAO.get(idTechno).toJsonObject())
                .build();
    }
    
    public JsonObject toJsonObjectLight() throws IOException
    {
        CinemaDAO CDAO = new CinemaDAO();
        TechnoDAO TechDAO = new TechnoDAO();
        return createObjectBuilder()
                .add("id", this.idSalle)
                .add("name", this.nom)
                .add("placelenght", this.nbPlace)
                .add("techno",TechDAO.get(idTechno).toJsonObject())
                .build();
    }
}
