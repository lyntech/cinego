/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Model;

import static javax.json.Json.createObjectBuilder;
import javax.json.JsonObject;

/**
 *
 * @author TERUEL
 */
public class FilmsVideos {
    
    private String id;
    private String key;
    private String name;
    private String type;
    
    public FilmsVideos() {
    }

    public FilmsVideos(String id, String key, String name, String type) {
        this.id = id;
        this.key = key;
        this.name = name;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public JsonObject toJsonObject()
    {
        return createObjectBuilder()
                .add("id", this.id)
                .add("key", this.key)
                .add("name", this.name)
                .add("type", this.type)
                .build();
    }
}
