/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.json.Json;
import static javax.json.Json.createObjectBuilder;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;

/**
 *
 * @author Utilisateur
 */
public class Enseigne {
    private int idEnseigne;
    private String nom;
    private List<Cinema> lesCinemas;
    
    /**
     *
     */
    public Enseigne()
    {
        nom = "newEnseigne";
        lesCinemas = new ArrayList<>();
    }
    
    /**
     *
     * @param unNom
     */
    public Enseigne(String unNom)
    {
        nom = unNom;
        lesCinemas = new ArrayList<>();
    }
    
    /**
     *
     * @param unIDEnseigne
     * @param unNom
     */
    public Enseigne(int unIDEnseigne,String unNom)
    {
        idEnseigne = unIDEnseigne;
        nom = unNom;
        lesCinemas = new ArrayList<>();
    }
    
    /**
     *
     * @param unIDEnseigne
     * @param unNom
     * @param uneListeCinemas
     */
    public Enseigne(int unIDEnseigne,String unNom,List<Cinema> uneListeCinemas)
    {
        idEnseigne = unIDEnseigne;
        nom = unNom;
        lesCinemas = uneListeCinemas;
    }

    /**
     * @return the idEnseigne
     */
    public int getIdEnseigne() {
        return idEnseigne;
    }

    /**
     * @param uneEnseigne
     */
    public void setIdEnseigne(int uneEnseigne) {
        idEnseigne = uneEnseigne;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param unNom
     */
    public void setNom(String unNom) {
        nom = unNom;
    }
    
    /**
     *
     * @return
     */
    public List<Cinema> getLesCinemas() {
        return lesCinemas;
    }

    /**
     *
     * @param uneListeCinemas
     */
    public void setLesCinemas(List<Cinema> uneListeCinemas) {
        lesCinemas = uneListeCinemas;
    }
    
    public JsonObject toJsonObject() throws IOException
    {
        return createObjectBuilder()
            .add("id", this.idEnseigne)
            .add("name", this.nom)
            .build();
    }
}
