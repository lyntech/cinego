/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Model;

import com.lyntech.cinegoapi.DAO.EnseigneDAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.json.Json;
import static javax.json.Json.createObjectBuilder;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author Utilisateur
 */
public class Cinema {
    private int idCinema;
    private String nom;
    private String adresse;
    private int idEnseigne;
    private List<Tarif> lesTarifs;
    private List<Salle> lesSalles;
    
    /**
     *
     */
    public Cinema()
    {
        lesSalles = new ArrayList<>();
    }
    
    /**
     *
     * @param unNom
     * @param uneAdresse
     */
    public Cinema(String unNom, String uneAdresse) {
        nom = unNom;
        adresse = uneAdresse;
    }
    
    /**
     *
     * @param unNom
     * @param uneAdresse
     * @param unIDEnseigne
     */
    public Cinema(String unNom,String uneAdresse, int unIDEnseigne)
    {
        nom = unNom;
        adresse = uneAdresse;
        idEnseigne = unIDEnseigne;
    }
    
    /**
     *
     * @param unIDCinema
     * @param unNom
     * @param uneAdresse
     * @param unIDEnseigne
     */
    public Cinema(int unIDCinema,String unNom,String uneAdresse, int unIDEnseigne)
    {
        idCinema = unIDCinema;
        nom = unNom;
        adresse = uneAdresse;
        idEnseigne = unIDEnseigne;
    }
    
    public Cinema(int unIDCinema,String unNom,String uneAdresse,int unIDEnseigne,List<Tarif> uneListeTarifs,List<Salle> uneListeSalles)
    {
        idCinema = unIDCinema;
        nom = unNom;
        adresse = uneAdresse;
        idEnseigne = unIDEnseigne;
        lesTarifs = uneListeTarifs;
        lesSalles = uneListeSalles;
    }

    /**
     *
     * @return
     */
    public int getIdCinema() 
    {
        return idCinema;
    }

    /**
     *
     * @param unidCinema
     */
    public void setIdCinema(int unidCinema) 
    {
        idCinema = unidCinema;
    }

    /**
     *
     * @return
     */
    public String getNom() 
    {
        return nom;
    }

    /**
     *
     * @param unNom
     */
    public void setNom(String unNom) 
    {
        nom = unNom;
    }

    /**
     *
     * @return
     */
    public String getAdresse() 
    {
        return adresse;
    }

    /**
     *
     * @param uneAdresse
     */
    public void setAdresse(String uneAdresse) 
    {
        adresse = uneAdresse;
    }

    /**
     *
     * @return
     */
    public int getidEnseigne() 
    {
        return idEnseigne;
    }

    /**
     *
     * @param unIDEnseigne
     */
    public void setEnseigne(int unIDEnseigne) 
    {
        idEnseigne = unIDEnseigne;
    }

    /**
     *
     * @return
     */
    public List<Tarif> getLesTarifs() 
    {
        return lesTarifs;
    }

    /**
     *
     * @param lesTarifs
     */
    public void setLesTarifs(List<Tarif> lesTarifs) 
    {
        this.lesTarifs = lesTarifs;
    }

    public List<Salle> getLesSalles() 
    {
        return lesSalles;
    }

    public void setLesSalles(List<Salle> lesSalles) 
    {
        this.lesSalles = lesSalles;
    }
    
    public JsonObject toJsonObject() throws IOException
    {
        EnseigneDAO EDAO = new EnseigneDAO();
        return createObjectBuilder()
               .add("id", this.idCinema)
               .add("name", this.nom)
               .add("adress", this.adresse)
               .add("signboard",EDAO.get(idEnseigne).toJsonObject())
               .build();
        /*if(!Enfant)
        {
            JsonArrayBuilder TarifBuilder = Json.createArrayBuilder();
            JsonArrayBuilder SalleBuilder = Json.createArrayBuilder(); 
            this.lesTarifs.forEach((unTarif)->{
                TarifBuilder.add(unTarif.toJsonObject());
            });
            this.lesSalles.forEach((uneSalle)->{
                SalleBuilder.add(uneSalle.toJsonObject());
            });
            JOBJ = createObjectBuilder()
                    .add("id", this.idEnseigne)
                    .add("name", this.nom)
                    .add("adress", this.adresse)
                    .add("iDEnseigne",this.idEnseigne)
                    .add("salles",SalleBuilder)
                    .add("tarifs", TarifBuilder)
                    .build();
        }
        return JOBJ;*/
    }
    
    public JsonObjectBuilder toJsonObjectLight() throws IOException
    {
        return createObjectBuilder()
               .add("id", this.idCinema)
               .add("name", this.nom)
               .add("adress", this.adresse);
    }
}
