/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Model;

import com.lyntech.cinegoapi.DAO.SalleDAO;
import com.lyntech.cinegoapi.DAO.SeanceDAO;
import com.lyntech.cinegoapi.Utils.WS.MovieDB;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import static javax.json.Json.createObjectBuilder;
import javax.json.JsonObject;

/**
 *
 * @author Yoann
 */
public class Seance {
    public int idSeance;
    public String date;
    public int idFilm;
    public int idSalle;
    // public List<Reservation> LesReservations;
    
    public Seance(int idSeance, String date, int idFilm, int idSalle) {
        this.idSeance = idSeance;
        this.date = date;
        this.idFilm = idFilm;
        this.idSalle = idSalle;
    }

    public Seance(String date, int idFilm, int idSalle) {
        this.date = date;
        this.idFilm = idFilm;
        this.idSalle = idSalle;
    }

    public int getIdSeance() {
        return idSeance;
    }

    public void setIdSeance(int idSeance) {
        this.idSeance = idSeance;
    }
    
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getIdFilm() {
        return idFilm;
    }

    public void setIdFilm(int idFilm) {
        this.idFilm = idFilm;
    }

    public int getIdSalle() {
        return idSalle;
    }

    public void setIdSalle(int idSalle) {
        this.idSalle = idSalle;
    }
    public JsonObject toJsonObject() throws IOException
    {
        SalleDAO SDAO = new SalleDAO();
        MovieDB MDB = new MovieDB();
        return createObjectBuilder()
                .add("id", this.idSeance)
                .add("date", this.date)
                .add("movie", MDB.getFilmsInfos(idFilm).toJsonObject())
                .add("room", SDAO.get(idSalle).toJsonObject())
                .build();
    }
    public JsonObject toJsonObjectLight() throws IOException
    {
        SalleDAO SDAO = new SalleDAO();
        return createObjectBuilder()
                .add("id", this.idSeance)
                .add("date", this.date)
                .add("room", SDAO.get(idSalle).toJsonObjectLight())
                .build();
    }
}
