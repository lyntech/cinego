package com.lyntech.cinegoapi.Model;

import static javax.json.Json.createObjectBuilder;
import javax.json.JsonValue;

/**
 * User Model
 * @author manug
 */
public class User {
    private int idUser;
    private String nom;
    private String prenom;
    private String pass;
    private int rank;
    private String login;


    public User() {
    }

    public User(int idUser, String login, String nom, String prenom, String pass, int rank) {
        this.idUser = idUser;
        this.login = login;
        this.nom = nom;
        this.prenom = prenom;
        this.pass = pass;
        this.rank = rank;
    }

    public User(int idUser, String login, String nom, String prenom, String pass) {
        this(idUser, login, nom, prenom, pass, 1);
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public JsonValue toJsonObject() {
        return createObjectBuilder()
                .add("id", this.idUser)
                .add("login", this.login)
                .add("nom", this.nom)
                .add("prenom", this.prenom)
                .add("rank", this.rank).build();
    }
      
}
