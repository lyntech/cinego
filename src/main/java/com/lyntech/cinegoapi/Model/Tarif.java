/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Model;

import com.lyntech.cinegoapi.DAO.CinemaDAO;
import java.io.IOException;
import static javax.json.Json.createObjectBuilder;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;

/**
 *
 * @author Utilisateur
 */
public class Tarif {
    private int idTarif;
    private String nom;
    private double prix;
    private int idCinema;
    
    /**
     *
     */
    public Tarif()
    {
    }
    
    /**
     *
     * @param unNom
     * @param unPrix
     * @param unIDCinema
     */
    public Tarif(String unNom, double unPrix,int unIDCinema)
    {
        nom = unNom;
        prix = unPrix;
        idCinema = unIDCinema;
    }
    
    /**
     *
     * @param unID
     * @param unNom
     * @param unPrix
     * @param unIDCinema
     */
    public Tarif(int unID,String unNom,double unPrix,int unIDCinema)
    {
        idTarif = unID;
        nom = unNom;
        prix = unPrix;
        idCinema = unIDCinema;
    }
    
    /**
     *
     * @return
     */
    public String getNom()
    {
        return nom;
    }
    
    /**
     *
     * @return
     */
    public double getPrix()
    {
        return prix;
    }
    
    /**
     *
     * @param unNom
     */
    public void setNom(String unNom)
    {
        nom = unNom;
    }
    
    /**
     *
     * @param unPrix
     */
    public void setPrix(double unPrix)
    {
        prix = unPrix;
    }
        
    /**
     *
     * @param unTarif
     */
    public void setIDTarif(int unTarif)
    {
        idTarif = unTarif;
    }
    
    /**
     *
     * @return
     */
    public int getIDTarif()
    {
        return idTarif;
    }
    
    /**
     *
     * @param unIDCinema
     */
    public void setIDCinema(int unIDCinema)
    {
        idCinema = unIDCinema;
    }
    
    /**
     *
     * @return
     */
    public int getIDcinema()
    {
        return idCinema;
    }
    
    public JsonObject toJsonObject() throws IOException
    {
        CinemaDAO CDAO = new CinemaDAO();
        return createObjectBuilder()
                .add("id", this.idTarif)
                .add("name", this.nom)
                .add("price", this.prix)
                .add("cinema", CDAO.get(this.idCinema).toJsonObject())
                .build();
    }
}
