/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Model;

import java.util.Date;
import static javax.json.Json.createObjectBuilder;
import javax.json.JsonValue;

/**
 *
 * @author Utilisateur
 */
public class PlaceReserver {
    public int idPlace;
    public int idReservation;
    public double prix;
    
    public PlaceReserver()
    {
        
    }
    
    public PlaceReserver(int idReservation,int idUtilisateur,double prix)
    {
        this.idPlace = idReservation;
        this.idReservation = idUtilisateur;
        this.prix = prix;
    }

    public int getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(int idPlace) {
        this.idPlace = idPlace;
    }

    public int getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(int idReservation) {
        this.idReservation = idReservation;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }
    
    public JsonValue toJsonObject() {
        return createObjectBuilder().add("idPlace", this.idReservation).add("idReservation", this.idReservation).add("prix", this.prix).build();
    }
}
