/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Model;

import javax.json.JsonObject;
import java.util.List;

import static javax.json.Json.createObjectBuilder;

/**
 *
 * @author TERUEL
 */
public class Techno {

    private int idTechno;
    private String nom;
    private List<Salle> LesSalles;

    public Techno(){

    }

    public Techno(String nom) {
        this.nom = nom;
    }

    public Techno(int idTechno, String nom) {
        this.idTechno = idTechno;
        this.nom = nom;
    }

    public Techno(int idTechno, String nom, List<Salle> lesSalles){
        this.idTechno = idTechno;
        this.nom = nom;
        this.LesSalles = lesSalles;
    }

    public int getIdTechno() {
        return idTechno;
    }

    public void setIdTechno(int idTechno) {
        this.idTechno = idTechno;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Salle> getLesSalles() {
        return LesSalles;
    }

    public void setLesSalles(List<Salle> lesSalles) {
        LesSalles = lesSalles;
    }

    public JsonObject toJsonObject()
    {
        return createObjectBuilder()
                .add("id", this.idTechno)
                .add("name", this.nom)
                .build();
    }
}
