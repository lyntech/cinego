/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Model;

import static javax.json.Json.createObjectBuilder;
import javax.json.JsonObject;

/**
 *
 * @author You
 */
public class FilmsCasts {
    private int cast_id;
    private String character;
    private int id;
    private String name;
    private String profile_path;
    
    public FilmsCasts() {
    }

    public FilmsCasts(int cast_id, String character, int id, String name, String profile_path) {
        this.cast_id = cast_id;
        this.character = character;
        this.id = id;
        this.name = name;
        this.profile_path = profile_path;
    }

    public int getCast_id() {
        return cast_id;
    }

    public void setCast_id(int cast_id) {
        this.cast_id = cast_id;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }
    
    public JsonObject toJsonObject()
    {
        return createObjectBuilder()
                .add("cast_id", this.cast_id)
                .add("character", this.character)
                .add("id", this.id)
                .add("name", this.name)
                .add("profile_path", this.profile_path)
                .build();
    }
}
