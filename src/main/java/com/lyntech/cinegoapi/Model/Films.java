package com.lyntech.cinegoapi.Model;

import static javax.json.Json.createObjectBuilder;
import javax.json.JsonObject;

/**
 * Films Model
 * @author manug
 */
public class Films {
    private int id;
    private String title;
    private String date;
    private String poster;
    private String summary;

    public Films() {
    }

    public Films(int id, String title, String date, String poster, String summary) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.poster = poster;
        this.summary = summary;
    }
    
    public Films(int id, String title, String date, String poster) {
        this(id, title, date, poster, null);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
    public JsonObject toJsonObject()
    {
        return createObjectBuilder()
                .add("id", this.id)
                .add("title", this.title)
                .add("date", this.date)
                .add("poster", this.poster)
                .add("summary", this.summary)
                .build();
    }
}
