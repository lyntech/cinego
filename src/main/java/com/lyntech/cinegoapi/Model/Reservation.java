/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyntech.cinegoapi.Model;

import com.lyntech.cinegoapi.DAO.ReservationDAO;
import com.lyntech.cinegoapi.DAO.SeanceDAO;
import com.lyntech.cinegoapi.DAO.UserDAO;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.sql.Date;
import javax.json.Json;
import static javax.json.Json.createObjectBuilder;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;

/**
 *
 * @author Yoann
 */
public class Reservation {
    public int idReservation;
    public int idUtilisateur;
    public String date;
    public int idSeance;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");;

    public Reservation(int idReservation, int idUtilisateur, String date, int idSeance) {
        this.idReservation = idReservation;
        this.idUtilisateur = idUtilisateur;
        this.date = date;
        this.idSeance = idSeance;
    }
    
    public Reservation(int idUtilisateur, String date, int idSeance) {
        this.idUtilisateur = idUtilisateur;
        this.date = date;
        this.idSeance = idSeance;
    }
    public int getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(int idReservation) {
        this.idReservation = idReservation;
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getIdSeance() {
        return idSeance;
    }

    public void setIdSeance(int idSeance) {
        this.idSeance = idSeance;
    }
    
    public JsonObject toJsonObject() throws IOException
    {
        UserDAO UDAO = new UserDAO();
        SeanceDAO SeDAO = new SeanceDAO();
        ReservationDAO RDAO = new ReservationDAO();
        JsonArrayBuilder PlaceBuilder = Json.createArrayBuilder();
        RDAO.getPlacesReservation(idReservation).forEach((unePlace)->{
            PlaceBuilder.add(unePlace.toJsonObject());
        });
        return createObjectBuilder()
                .add("id", this.idReservation)
                .add("user", UDAO.get(idUtilisateur).toJsonObject())
                .add("date", this.date)
                .add("sceance", SeDAO.get(idSeance).toJsonObject())
                .add("places", PlaceBuilder)
                .build();
    }
}
