-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 13 mars 2019 à 09:01
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `cinego`
--

-- --------------------------------------------------------

--
-- Structure de la table `cinema`
--

DROP TABLE IF EXISTS `cinema`;
CREATE TABLE IF NOT EXISTS `cinema` (
  `idCinema` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `idEnseigne` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCinema`),
  KEY `ref_cinema_enseigne` (`idEnseigne`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `enseigne`
--

DROP TABLE IF EXISTS `enseigne`;
CREATE TABLE IF NOT EXISTS `enseigne` (
  `idEnseigne` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idEnseigne`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `fidelite`
--

DROP TABLE IF EXISTS `fidelite`;
CREATE TABLE IF NOT EXISTS `fidelite` (
  `idFidelite` int(11) NOT NULL AUTO_INCREMENT,
  `numFidelite` char(10) NOT NULL,
  `idUtilisateur` int(11) NOT NULL,
  `idEnseigne` int(11) NOT NULL,
  PRIMARY KEY (`idFidelite`),
  KEY `index_fidelite_utilisateur` (`idUtilisateur`),
  KEY `ref_fidelite_enseigne` (`idEnseigne`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `placereserver`
--

DROP TABLE IF EXISTS `placereserver`;
CREATE TABLE IF NOT EXISTS `placereserver` (
  `idPlace` int(11) NOT NULL,
  `idReservation` int(11) NOT NULL,
  `prix` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`idPlace`,`idReservation`),
  KEY `ref_plreserv_resa` (`idReservation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `idReservation` int(11) NOT NULL AUTO_INCREMENT,
  `idUtilisateur` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `idSeance` int(11) DEFAULT NULL,
  PRIMARY KEY (`idReservation`),
  KEY `ref_resa_utilisateur` (`idUtilisateur`),
  KEY `ref_resa_seance` (`idSeance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

DROP TABLE IF EXISTS `salle`;
CREATE TABLE IF NOT EXISTS `salle` (
  `idSalle` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `plan` blob,
  `nbPlace` int(11) DEFAULT NULL,
  `idCinema` int(11) DEFAULT NULL,
  `idTechno` int(11) DEFAULT NULL,
  PRIMARY KEY (`idSalle`),
  KEY `ref_salle_cinema` (`idCinema`),
  KEY `ref_salle_techno` (`idTechno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `seance`
--

DROP TABLE IF EXISTS `seance`;
CREATE TABLE IF NOT EXISTS `seance` (
  `idSeance` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `idFilm` int(11) DEFAULT NULL,
  `idSalle` int(11) DEFAULT NULL,
  PRIMARY KEY (`idSeance`),
  KEY `ref_seance_salle` (`idSalle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tarif`
--

DROP TABLE IF EXISTS `tarif`;
CREATE TABLE IF NOT EXISTS `tarif` (
  `idTarif` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `prix` decimal(10,2) DEFAULT NULL,
  `idCinema` int(11) DEFAULT NULL,
  PRIMARY KEY (`idTarif`),
  KEY `ref_tarif_cinema` (`idCinema`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `technologie`
--

DROP TABLE IF EXISTS `technologie`;
CREATE TABLE IF NOT EXISTS `technologie` (
  `idTechno` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idTechno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `grade` int(11) NOT NULL,
  PRIMARY KEY (`idUtilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `cinema`
--
ALTER TABLE `cinema`
  ADD CONSTRAINT `ref_cinema_enseigne` FOREIGN KEY (`idEnseigne`) REFERENCES `enseigne` (`idEnseigne`);

--
-- Contraintes pour la table `fidelite`
--
ALTER TABLE `fidelite`
  ADD CONSTRAINT `ref_fidelite_enseigne` FOREIGN KEY (`idEnseigne`) REFERENCES `enseigne` (`idEnseigne`),
  ADD CONSTRAINT `ref_fidelite_utilisateur` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`idUtilisateur`);

--
-- Contraintes pour la table `placereserver`
--
ALTER TABLE `placereserver`
  ADD CONSTRAINT `ref_plreserv_resa` FOREIGN KEY (`idReservation`) REFERENCES `reservation` (`idReservation`);

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `ref_resa_seance` FOREIGN KEY (`idSeance`) REFERENCES `seance` (`idSeance`),
  ADD CONSTRAINT `ref_resa_utilisateur` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`idUtilisateur`);

--
-- Contraintes pour la table `salle`
--
ALTER TABLE `salle`
  ADD CONSTRAINT `ref_salle_cinema` FOREIGN KEY (`idCinema`) REFERENCES `cinema` (`idCinema`),
  ADD CONSTRAINT `ref_salle_techno` FOREIGN KEY (`idTechno`) REFERENCES `technologie` (`idTechno`);

--
-- Contraintes pour la table `seance`
--
ALTER TABLE `seance`
  ADD CONSTRAINT `ref_seance_salle` FOREIGN KEY (`idSalle`) REFERENCES `salle` (`idSalle`);

--
-- Contraintes pour la table `tarif`
--
ALTER TABLE `tarif`
  ADD CONSTRAINT `ref_tarif_cinema` FOREIGN KEY (`idCinema`) REFERENCES `cinema` (`idCinema`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
