-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 13 mars 2019 à 08:46
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `cinego`
--

--
-- Déchargement des données de la table `enseigne`
--

INSERT INTO `enseigne` (`idEnseigne`, `nom`) VALUES
(1, 'StarEnseigne');

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`idUtilisateur`, `login`, `nom`, `prenom`, `pass`, `grade`) VALUES
(1, 'Switch@test.fr', 'Switch', 'Switch', '$2a$12$YqlZulrErSLcprni3Wa13.Oaft/pKwvylwfG/Rl/CnzrLbBrgs/Cy', 50);

--
-- Déchargement des données de la table `cinema`
--

INSERT INTO `cinema` (`idCinema`, `nom`, `adresse`, `idEnseigne`) VALUES
(1, 'CineOlisar', 'Port Olisar', 1),
(2, 'Cine Lorville', 'Lorville', 1);

--
-- Déchargement des données de la table `fidelite`
--

INSERT INTO `fidelite` (`idFidelite`, `numFidelite`, `idUtilisateur`, `idEnseigne`) VALUES
(1, '00012300', 1, 1);

--
-- Déchargement des données de la table `technologie`
--

INSERT INTO `technologie` (`idTechno`, `nom`) VALUES
(1, 'Dolby Cinema'),
(2, 'Dolby Cinema 3D'),
(3, 'Normal (Dolby Atmos)'),
(4, 'Normal 3D (Dolby Atmos)');

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`idSalle`, `nom`, `plan`, `nbPlace`, `idCinema`, `idTechno`) VALUES
(1, 'StarSalle1', 0x3c3f786d6c2076657273696f6e3d22312e302220656e636f64696e673d225554462d3822203f3e0d0a3c726f6f6d3e0d0a202020203c72616e67652069643d2231222073697a653d2233223e0d0a20202020202020203c706c6163652069643d22312220747970653d223022206c6f636174696f6e3d2231222f3e0d0a20202020202020203c706c6163652069643d22322220747970653d223122206c6f636174696f6e3d2232222f3e0d0a20202020202020203c706c6163652069643d22332220747970653d223222206c6f636174696f6e3d2233222f3e0d0a202020203c2f72616e67653e0d0a3c2f726f6f6d3e0d0a, 50, 1, 1),
(2, 'StarSalle2', 0x3c3f786d6c2076657273696f6e3d22312e302220656e636f64696e673d225554462d3822203f3e0d0a3c726f6f6d3e0d0a202020203c72616e67652069643d2231222073697a653d2233223e0d0a20202020202020203c706c6163652069643d22312220747970653d223022206c6f636174696f6e3d2231222f3e0d0a20202020202020203c706c6163652069643d22322220747970653d223122206c6f636174696f6e3d2232222f3e0d0a20202020202020203c706c6163652069643d22332220747970653d223222206c6f636174696f6e3d2233222f3e0d0a202020203c2f72616e67653e0d0a3c2f726f6f6d3e0d0a, 40, 1, 2);

--
-- Déchargement des données de la table `seance`
--

INSERT INTO `seance` (`idSeance`, `date`, `idFilm`, `idSalle`) VALUES
(1, '2019-03-14 18:30:32', 2164, 1),
(2, '2019-03-15 20:00:00', 2164, 2);

--
-- Déchargement des données de la table `tarif`
--

INSERT INTO `tarif` (`idTarif`, `nom`, `prix`, `idCinema`) VALUES
(1, 'StarTarif1', '10.00', 1),
(2, 'StarTarif2', '8.50', 1),
(8, 'StarTarif3', '11.50', 2),
(9, 'StarTarif4', '10.00', 2);

--
-- Déchargement des données de la table `reservation`
--

INSERT INTO `reservation` (`idReservation`, `idUtilisateur`, `date`, `idSeance`) VALUES
(1, 1, '2019-03-13 09:42:23', 1),
(2, 1, '2019-03-13 09:43:00', 2);

--
-- Déchargement des données de la table `placereserver`
--

INSERT INTO `placereserver` (`idPlace`, `idReservation`, `prix`) VALUES
(2, 1, '8.50'),
(2, 2, '11.00');
COMMIT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
